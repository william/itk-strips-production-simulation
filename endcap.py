import os
import random
import math
import simpy
import csv
import model
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
matplotlib.rc('xtick', labelsize=25)     
matplotlib.rc('ytick', labelsize=25)

OUTPUTDIR = 'endcap_output'
START_OFFSET = 60    



class ProductionSite(object):
    

    def __init__(self, env, name):
        self.name = name
        self.env = env
        # Inventory
        self.n_abcs = simpy.Container(env)
        self.n_hccs = simpy.Container(env)
        self.n_bare_r0h0 = simpy.Container(env)
        self.n_bare_r0h1 = simpy.Container(env)
        self.n_bare_r1h0 = simpy.Container(env)
        self.n_bare_r1h1 = simpy.Container(env)
        self.n_bare_r2h0 = simpy.Container(env)
        self.n_bare_r3h0 = simpy.Container(env)
        self.n_bare_r3h1 = simpy.Container(env)
        self.n_bare_r3h2 = simpy.Container(env)
        self.n_bare_r3h3 = simpy.Container(env)
        self.n_bare_r4h0 = simpy.Container(env)
        self.n_bare_r4h1 = simpy.Container(env)
        self.n_bare_r5h0 = simpy.Container(env)
        self.n_bare_r5h1 = simpy.Container(env)
        self.n_complete_r0h0 = simpy.Container(env)
        self.n_complete_r0h1 = simpy.Container(env)
        self.n_complete_r1h0 = simpy.Container(env)
        self.n_complete_r1h1 = simpy.Container(env)
        self.n_complete_r2h0 = simpy.Container(env)
        self.n_complete_r3h0 = simpy.Container(env)
        self.n_complete_r3h1 = simpy.Container(env)
        self.n_complete_r3h2 = simpy.Container(env)
        self.n_complete_r3h3 = simpy.Container(env)
        self.n_complete_r4h0 = simpy.Container(env)
        self.n_complete_r4h1 = simpy.Container(env)
        self.n_complete_r5h0 = simpy.Container(env)
        self.n_complete_r5h1 = simpy.Container(env)
        self.n_pre_burn_in_r0h0 = simpy.Container(env)
        self.n_pre_burn_in_r0h1 = simpy.Container(env)
        self.n_pre_burn_in_r1h0 = simpy.Container(env)
        self.n_pre_burn_in_r1h1 = simpy.Container(env)
        self.n_pre_burn_in_r2h0 = simpy.Container(env)
        self.n_pre_burn_in_r3h0 = simpy.Container(env)
        self.n_pre_burn_in_r3h1 = simpy.Container(env)
        self.n_pre_burn_in_r3h2 = simpy.Container(env)
        self.n_pre_burn_in_r3h3 = simpy.Container(env)
        self.n_pre_burn_in_r4h0 = simpy.Container(env)
        self.n_pre_burn_in_r4h1 = simpy.Container(env)
        self.n_pre_burn_in_r5h0 = simpy.Container(env)
        self.n_pre_burn_in_r5h1 = simpy.Container(env)
        self.n_r0_sensors = simpy.Container(env)
        self.n_r1_sensors = simpy.Container(env)
        self.n_r2_sensors = simpy.Container(env)
        self.n_r3_sensors = simpy.Container(env)
        self.n_r4_sensors = simpy.Container(env)
        self.n_r5_sensors = simpy.Container(env)
        self.n_r0_powerboards = simpy.Container(env)
        self.n_r1_powerboards = simpy.Container(env)
        self.n_r2_powerboards = simpy.Container(env)
        self.n_r3_powerboards = simpy.Container(env)
        self.n_r4_powerboards = simpy.Container(env)
        self.n_r5_powerboards = simpy.Container(env)
        #self.n_hybrid_glues = simpy.Container(env)
        self.n_r0_modules = simpy.Container(env)
        self.n_r1_modules = simpy.Container(env)
        self.n_r2_modules = simpy.Container(env)
        self.n_r3_modules = simpy.Container(env)
        self.n_r4_modules = simpy.Container(env)
        self.n_r5_modules = simpy.Container(env)
        self.n_petals = simpy.Container(env)
        
        # Production Settings
        self.rest_until_date = 0
        self.available_fte = 100
        self.hybrid_production_capacity = 0.0
        self.hybrid_production_sd = 0.0
        self.hybrid_burn_in_yield = 0.99
        self.module_production_capacity = 0.0
        self.module_production_sd = 0.0
        self.make_r0h0 = False
        self.make_r0h1 = False
        self.make_r1h0 = False
        self.make_r1h1 = False
        self.make_r2h0 = False
        self.make_r3h0 = False
        self.make_r3h1 = False
        self.make_r3h2 = False
        self.make_r3h3 = False
        self.make_r4h0 = False
        self.make_r4h1 = False
        self.make_r5h0 = False
        self.make_r5h1 = False
        self.r0h0_production_weight = 1.0
        self.r0h1_production_weight = 1.0
        self.r1h0_production_weight = 1.0
        self.r1h1_production_weight = 1.0
        self.r2h0_production_weight = 1.0
        self.r3h0_production_weight = 1.0
        self.r3h1_production_weight = 1.0
        self.r3h2_production_weight = 1.0
        self.r3h3_production_weight = 1.0
        self.r4h0_production_weight = 1.0
        self.r4h1_production_weight = 1.0
        self.r5h0_production_weight = 1.0
        self.r5h1_production_weight = 1.0
        self.r0h0_yield = 1.0
        self.r0h1_yield = 1.0
        self.r1h0_yield = 1.0
        self.r1h1_yield = 1.0
        self.r2h0_yield = 1.0
        self.r3h0_yield = 1.0
        self.r3h1_yield = 1.0
        self.r3h2_yield = 1.0
        self.r3h3_yield = 1.0
        self.r4h0_yield = 1.0
        self.r4h1_yield = 1.0
        self.r5h0_yield = 1.0
        self.r5h1_yield = 1.0
        self.make_r0_modules = False
        self.make_r1_modules = False
        self.make_r2_modules = False
        self.make_r3_modules = False
        self.make_r4_modules = False
        self.make_r5_modules = False
        self.r0_production_weight= 1.0
        self.r1_production_weight= 1.0
        self.r2_production_weight= 1.0
        self.r3_production_weight= 1.0
        self.r4_production_weight= 1.0
        self.r5_production_weight= 1.0
        self.r0_module_yield = 1.0
        self.r1_module_yield = 1.0
        self.r2_module_yield = 1.0
        self.r3_module_yield = 1.0
        self.r4_module_yield = 1.0
        self.r5_module_yield = 1.0
        
        # Tracking Total Items Produced
        self.total_complete_r0h0_made = 0
        self.total_complete_r0h1_made = 0
        self.total_complete_r1h0_made = 0
        self.total_complete_r1h1_made = 0
        self.total_complete_r2h0_made = 0
        self.total_complete_r3h0_made = 0
        self.total_complete_r3h1_made = 0
        self.total_complete_r3h2_made = 0
        self.total_complete_r3h3_made = 0
        self.total_complete_r4h0_made = 0
        self.total_complete_r4h1_made = 0
        self.total_complete_r5h0_made = 0
        self.total_complete_r5h1_made = 0
        self.total_r0_modules_made = 0
        self.total_r1_modules_made = 0
        self.total_r2_modules_made = 0
        self.total_r3_modules_made = 0
        self.total_r4_modules_made = 0
        self.total_r5_modules_made = 0
        
        # shipping things
        
        self.n_complete_r0h0_made_today = 0
        self.n_complete_r0h1_made_today = 0
        self.n_complete_r1h0_made_today = 0
        self.n_complete_r1h1_made_today = 0
        self.n_complete_r2h0_made_today = 0
        self.n_complete_r3h0_made_today = 0
        self.n_complete_r3h1_made_today = 0
        self.n_complete_r3h2_made_today = 0
        self.n_complete_r3h3_made_today = 0
        self.n_complete_r4h0_made_today = 0
        self.n_complete_r4h1_made_today = 0
        self.n_complete_r5h0_made_today = 0
        self.n_complete_r5h1_made_today = 0
        self.n_r0_modules_made_today = 0
        self.n_r1_modules_made_today = 0
        self.n_r2_modules_made_today = 0
        self.n_r3_modules_made_today = 0
        self.n_r4_modules_made_today = 0
        self.n_r5_modules_made_today = 0
        
        self.n_complete_r0h0_to_make = 0
        self.n_complete_r0h1_to_make = 0
        self.n_complete_r1h0_to_make = 0
        self.n_complete_r1h1_to_make = 0
        self.n_complete_r2h0_to_make = 0
        self.n_complete_r3h0_to_make = 0
        self.n_complete_r3h1_to_make = 0
        self.n_complete_r3h2_to_make = 0
        self.n_complete_r3h3_to_make = 0
        self.n_complete_r4h0_to_make = 0
        self.n_complete_r4h1_to_make = 0
        self.n_complete_r5h0_to_make = 0
        self.n_complete_r5h1_to_make = 0
        self.n_hybrids_to_make = 0
        self.n_r0_modules_to_make = 0
        self.n_r1_modules_to_make = 0
        self.n_r2_modules_to_make = 0
        self.n_r3_modules_to_make = 0
        self.n_r4_modules_to_make = 0
        self.n_r5_modules_to_make = 0
        self.n_modules_to_make = 0
        
        
        
    def MakeParts(self):
        
        while True:

            # Don't work if it's the weekend
            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)

            else:

                n_types_of_hybrids = self.GetNumberOfHybridTypes()
                n_types_of_modules = self.GetNumberOfModuleTypes()


                #####################
                # Module production #
                #####################

                if self.module_production_capacity > 0.01:

                    r0_module_capacity_reached = False
                    if self.make_r0_modules == False:
                        r0_module_capacity_reached = True

                    r1_module_capacity_reached = False
                    if self.make_r1_modules == False:
                        r1_module_capacity_reached = True

                    r2_module_capacity_reached = False
                    if self.make_r2_modules == False:
                        r2_module_capacity_reached = True

                    r3_module_capacity_reached = False
                    if self.make_r3_modules == False:
                        r3_module_capacity_reached = True

                    r4_module_capacity_reached = False
                    if self.make_r4_modules == False:
                        r4_module_capacity_reached = True

                    r5_module_capacity_reached = False
                    if self.make_r5_modules == False:
                        r5_module_capacity_reached = True

                    n_r0_modules_made = 0.0
                    n_r1_modules_made = 0.0
                    n_r2_modules_made = 0.0
                    n_r3_modules_made = 0.0
                    n_r4_modules_made = 0.0
                    n_r5_modules_made = 0.0

                    if self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0 > self.available_fte:
                        corrected_production_capacity = ( (self.module_production_capacity*self.available_fte) 
                                                          / (self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0) )
                    else:
                        corrected_production_capacity = self.module_production_capacity
                    actual_production_capacity = random.gauss(corrected_production_capacity, self.module_production_sd)
                    leftover_production_capacity = actual_production_capacity

                    self.n_modules_to_make += (self.n_r0_modules_to_make + self.n_r1_modules_to_make + self.n_r2_modules_to_make 
                                            + self.n_r3_modules_to_make + self.n_r4_modules_to_make + self.n_r5_modules_to_make)

                    self.n_r0_modules_to_make = 0
                    self.n_r1_modules_to_make = 0
                    self.n_r2_modules_to_make = 0
                    self.n_r3_modules_to_make = 0
                    self.n_r4_modules_to_make = 0
                    self.n_r5_modules_to_make = 0

                    if self.n_modules_to_make > 0:
                        if self.n_modules_to_make > leftover_production_capacity:
                            self.n_modules_to_make -= leftover_production_capacity
                            leftover_production_capacity = 0
                        else:
                            leftover_production_capacity -= self.n_modules_to_make
                            self.n_modules_to_make = 0

                    while (leftover_production_capacity > 0.01 and (r0_module_capacity_reached == False or r1_module_capacity_reached == False or
                                                                    r2_module_capacity_reached == False or r3_module_capacity_reached == False or
                                                                    r4_module_capacity_reached == False or r5_module_capacity_reached == False)):

                        leftover_production_capacity = (actual_production_capacity -
                                                        n_r0_modules_made - n_r1_modules_made -
                                                        n_r2_modules_made - n_r3_modules_made -
                                                        n_r4_modules_made - n_r5_modules_made)

                        total_n_modules_made = 0

                        if self.make_r0_modules == True:
                            total_n_modules_made += (self.total_r0_modules_made + n_r0_modules_made)/(self.r0_production_weight)
                        if self.make_r1_modules == True:
                            total_n_modules_made += (self.total_r1_modules_made + n_r1_modules_made)/(self.r1_production_weight)
                        if self.make_r2_modules == True:
                            total_n_modules_made += (self.total_r2_modules_made + n_r2_modules_made)/(self.r2_production_weight)
                        if self.make_r3_modules == True:
                            total_n_modules_made += (self.total_r3_modules_made + n_r3_modules_made)/self.r3_production_weight
                        if self.make_r4_modules == True:
                            total_n_modules_made += (self.total_r4_modules_made + n_r4_modules_made)/self.r4_production_weight
                        if self.make_r5_modules == True:
                            total_n_modules_made += (self.total_r5_modules_made + n_r5_modules_made)/self.r5_production_weight

                        average_n_modules_made = total_n_modules_made/n_types_of_modules

                        # Make more of any modules that are below average in total number produced
                        while (((self.total_r0_modules_made + n_r0_modules_made)/(self.r0_production_weight) < average_n_modules_made and r0_module_capacity_reached == False)
                                or ((self.total_r1_modules_made + n_r1_modules_made)/(self.r1_production_weight) < average_n_modules_made and r1_module_capacity_reached == False)
                                or ((self.total_r2_modules_made + n_r2_modules_made)/(self.r2_production_weight) < average_n_modules_made and r2_module_capacity_reached == False)
                                or ((self.total_r3_modules_made + n_r3_modules_made)/self.r3_production_weight < average_n_modules_made and r3_module_capacity_reached == False)
                                or ((self.total_r4_modules_made + n_r4_modules_made)/self.r4_production_weight < average_n_modules_made and r4_module_capacity_reached == False)
                                or ((self.total_r5_modules_made + n_r5_modules_made)/self.r5_production_weight < average_n_modules_made and r5_module_capacity_reached == False)):

                            total_n_modules_made = 0

                            if self.make_r0_modules == True:
                                total_n_modules_made += (self.total_r0_modules_made + n_r0_modules_made)/(self.r0_production_weight)
                            if self.make_r1_modules == True:
                                total_n_modules_made += (self.total_r1_modules_made + n_r1_modules_made)/(self.r1_production_weight)
                            if self.make_r2_modules == True:
                                total_n_modules_made += (self.total_r2_modules_made + n_r2_modules_made)/(self.r2_production_weight)
                            if self.make_r3_modules == True:
                                total_n_modules_made += (self.total_r3_modules_made + n_r3_modules_made)/self.r3_production_weight
                            if self.make_r4_modules == True:
                                total_n_modules_made += (self.total_r4_modules_made + n_r4_modules_made)/self.r4_production_weight
                            if self.make_r5_modules == True:
                                total_n_modules_made += (self.total_r5_modules_made + n_r5_modules_made)/self.r5_production_weight

                            average_n_modules_made = total_n_modules_made/n_types_of_modules

                            # Try to make one more R0 Module
                            if r0_module_capacity_reached == False and (self.total_r0_modules_made + n_r0_modules_made)/(self.r0_production_weight) < average_n_modules_made:
                                if (n_r0_modules_made + 1 > self.n_r0_sensors.level or
                                    n_r0_modules_made + 1 > self.n_r0_powerboards.level or
                                    n_r0_modules_made + 1 > self.n_complete_r0h0.level or
                                    n_r0_modules_made + 1 > self.n_complete_r0h1.level):
                                    #n_r0_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r0_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r0_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r0_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R1 Module
                            if r1_module_capacity_reached == False and (self.total_r1_modules_made + n_r1_modules_made)/(self.r1_production_weight) < average_n_modules_made:
                                if (n_r1_modules_made + 1 > self.n_r1_sensors.level or
                                    n_r1_modules_made + 1 > self.n_r1_powerboards.level or
                                    n_r1_modules_made + 1 > self.n_complete_r1h0.level or
                                    n_r1_modules_made + 1 > self.n_complete_r1h1.level):
                                    #n_r1_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r1_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r1_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r1_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R2 Module
                            if r2_module_capacity_reached == False and (self.total_r2_modules_made + n_r2_modules_made)/(self.r2_production_weight) < average_n_modules_made:
                                if (n_r2_modules_made + 1 > self.n_r2_sensors.level or
                                    n_r2_modules_made + 1 > self.n_r2_powerboards.level or
                                    n_r2_modules_made + 1 > self.n_complete_r2h0.level):
                                    #n_r2_modules_made + 1 > self.n_hybrid_glues.level):
                                    r2_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r2_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r2_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3 Module
                            if r3_module_capacity_reached == False and (self.total_r3_modules_made + n_r3_modules_made)/self.r3_production_weight < average_n_modules_made:
                                if (n_r3_modules_made + 1 > int(self.n_r3_sensors.level/2) or
                                    n_r3_modules_made + 1 > self.n_r3_powerboards.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h0.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h1.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h2.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h3.level):
                                    #n_r3_modules_made + 1 > int(self.n_hybrid_glues.level/4)):
                                    r3_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 2:
                                        n_r3_modules_made += 2
                                        leftover_production_capacity -= 2
                                    else:
                                        n_r3_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R4 Module
                            if r4_module_capacity_reached == False and (self.total_r4_modules_made + n_r4_modules_made)/self.r4_production_weight < average_n_modules_made:
                                if (n_r4_modules_made + 1 > int(self.n_r4_sensors.level/2) or
                                    n_r4_modules_made + 1 > self.n_r4_powerboards.level or
                                    n_r4_modules_made + 1 > self.n_complete_r4h0.level or
                                    n_r4_modules_made + 1 > self.n_complete_r4h1.level):
                                    #n_r4_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r4_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 2:
                                        n_r4_modules_made += 2
                                        leftover_production_capacity -= 2
                                    else:
                                        n_r4_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R5 Module
                            if r5_module_capacity_reached == False and (self.total_r5_modules_made + n_r5_modules_made)/self.r5_production_weight < average_n_modules_made:
                                if (n_r5_modules_made + 1 > int(self.n_r5_sensors.level/2) or
                                    n_r5_modules_made + 1 > self.n_r5_powerboards.level or
                                    n_r5_modules_made + 1 > self.n_complete_r5h0.level or
                                    n_r5_modules_made + 1 > self.n_complete_r5h1.level):
                                    #n_r5_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r5_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 2:
                                        n_r5_modules_made += 2
                                        leftover_production_capacity -= 2
                                    else:
                                        n_r5_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                        # If equal number of modules have been produced so far, then randomly select which ones to make one more of
                        random_list = list(range(0,6))
                        random.shuffle(random_list)

                        for itr in random_list:
                            # Try to make one more R0 Module
                            if r0_module_capacity_reached == False and itr == 0:
                                if (n_r0_modules_made + 1 > self.n_r0_sensors.level or
                                    n_r0_modules_made + 1 > self.n_r0_powerboards.level or
                                    n_r0_modules_made + 1 > self.n_complete_r0h0.level or
                                    n_r0_modules_made + 1 > self.n_complete_r0h1.level):
                                    #n_r0_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r0_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r0_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r0_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R1 Module
                            if r1_module_capacity_reached == False and itr == 1:
                                if (n_r1_modules_made + 1 > self.n_r1_sensors.level or
                                    n_r1_modules_made + 1 > self.n_r1_powerboards.level or
                                    n_r1_modules_made + 1 > self.n_complete_r1h0.level or
                                    n_r1_modules_made + 1 > self.n_complete_r1h1.level):
                                    #n_r1_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r1_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r1_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r1_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R2 Module
                            if r2_module_capacity_reached == False and itr == 2:
                                if (n_r2_modules_made + 1 > self.n_r2_sensors.level or
                                    n_r2_modules_made + 1 > self.n_r2_powerboards.level or
                                    n_r2_modules_made + 1 > self.n_complete_r2h0.level):
                                    #n_r2_modules_made + 1 > self.n_hybrid_glues.level):
                                    r2_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r2_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r2_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3 Module
                            if r3_module_capacity_reached == False and itr == 3:
                                if (n_r3_modules_made + 1 > int(self.n_r3_sensors.level/2) or
                                    n_r3_modules_made + 1 > self.n_r3_powerboards.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h0.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h1.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h2.level or
                                    n_r3_modules_made + 1 > self.n_complete_r3h3.level):
                                    #n_r3_modules_made + 1 > int(self.n_hybrid_glues.level/4)):
                                    r3_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 2:
                                        n_r3_modules_made += 2
                                        leftover_production_capacity -= 2
                                    else:
                                        n_r3_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R4 Module        
                            if r4_module_capacity_reached == False and itr == 4:
                                if (n_r4_modules_made + 1 > int(self.n_r4_sensors.level/2) or
                                    n_r4_modules_made + 1 > self.n_r4_powerboards.level or
                                    n_r4_modules_made + 1 > self.n_complete_r4h0.level or
                                    n_r4_modules_made + 1 > self.n_complete_r4h1.level):
                                    #n_r4_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r4_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 2:
                                        n_r4_modules_made += 2
                                        leftover_production_capacity -= 2
                                    else:
                                        n_r4_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R5 Module        
                            if r5_module_capacity_reached == False and itr == 5:
                                if (n_r5_modules_made + 1 > int(self.n_r5_sensors.level/2) or
                                    n_r5_modules_made + 1 > self.n_r5_powerboards.level or
                                    n_r5_modules_made + 1 > self.n_complete_r5h0.level or
                                    n_r5_modules_made + 1 > self.n_complete_r5h1.level):
                                    #n_r5_modules_made + 1 > int(self.n_hybrid_glues.level/2)):
                                    r5_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 2:
                                        n_r5_modules_made += 2
                                        leftover_production_capacity -= 2
                                    else:
                                        n_r5_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                    # Make the modules
                    if n_r0_modules_made > 0:
                        self.MakeR0Modules(n_r0_modules_made, self.r0_module_yield)
                    if n_r1_modules_made > 0:
                        self.MakeR1Modules(n_r1_modules_made, self.r1_module_yield)
                    if n_r2_modules_made > 0:
                        self.MakeR2Modules(n_r2_modules_made, self.r2_module_yield)
                    if n_r3_modules_made > 0:
                        self.MakeR3Modules(n_r3_modules_made, self.r3_module_yield)
                    if n_r4_modules_made > 0:
                        self.MakeR4Modules(n_r4_modules_made, self.r4_module_yield)
                    if n_r5_modules_made > 0:
                        self.MakeR5Modules(n_r5_modules_made, self.r5_module_yield)

                #####################
                # Hybrid production #
                #####################



                if self.hybrid_production_capacity > 0.01:

                    r0h0_capacity_reached = False
                    if self.make_r0h0 == False:
                        r0h0_capacity_reached = True

                    r0h1_capacity_reached = False                
                    if self.make_r0h1 == False:
                        r0h1_capacity_reached = True

                    r1h0_capacity_reached = False
                    if self.make_r1h0 == False:
                        r1h0_capacity_reached = True

                    r1h1_capacity_reached = False
                    if self.make_r1h1 == False:
                        r1h1_capacity_reached = True

                    r2h0_capacity_reached = False
                    if self.make_r2h0 == False:
                        r2h0_capacity_reached = True

                    r3h0_capacity_reached = False
                    if self.make_r3h0 == False:
                        r3h0_capacity_reached = True

                    r3h1_capacity_reached = False
                    if self.make_r3h1 == False:
                        r3h1_capacity_reached = True

                    r3h2_capacity_reached = False
                    if self.make_r3h2 == False:
                        r3h2_capacity_reached = True

                    r3h3_capacity_reached = False
                    if self.make_r3h3 == False:
                        r3h3_capacity_reached = True

                    r4h0_capacity_reached = False
                    if self.make_r4h0 == False:
                        r4h0_capacity_reached = True

                    r4h1_capacity_reached = False
                    if self.make_r4h1 == False:
                        r4h1_capacity_reached = True

                    r5h0_capacity_reached = False
                    if self.make_r5h0 == False:
                        r5h0_capacity_reached = True

                    r5h1_capacity_reached = False
                    if self.make_r5h1 == False:
                        r5h1_capacity_reached = True

                    n_r0h0s_made = 0
                    n_r0h1s_made = 0
                    n_r1h0s_made = 0
                    n_r1h1s_made = 0
                    n_r2h0s_made = 0
                    n_r3h0s_made = 0
                    n_r3h1s_made = 0
                    n_r3h2s_made = 0
                    n_r3h3s_made = 0
                    n_r4h0s_made = 0
                    n_r4h1s_made = 0
                    n_r5h0s_made = 0
                    n_r5h1s_made = 0

                    if self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0 > self.available_fte:
                        corrected_production_capacity = (self.hybrid_production_capacity*self.available_fte) / (self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0)
                    else:
                        corrected_production_capacity = self.hybrid_production_capacity
                    actual_production_capacity = random.gauss(corrected_production_capacity, self.hybrid_production_sd)
                    leftover_production_capacity = actual_production_capacity

                    self.n_hybrids_to_make += (self.n_complete_r0h0_to_make + self.n_complete_r0h1_to_make + self.n_complete_r1h0_to_make
                                               + self.n_complete_r1h1_to_make + self.n_complete_r2h0_to_make + self.n_complete_r3h0_to_make
                                               + self.n_complete_r3h1_to_make + self.n_complete_r3h2_to_make + self.n_complete_r3h3_to_make
                                               + self.n_complete_r4h0_to_make + self.n_complete_r4h1_to_make + self.n_complete_r5h0_to_make
                                               + self.n_complete_r5h1_to_make)


                    self.n_complete_r0h0_to_make = 0
                    self.n_complete_r0h1_to_make = 0
                    self.n_complete_r1h0_to_make = 0
                    self.n_complete_r1h1_to_make = 0
                    self.n_complete_r2h0_to_make = 0
                    self.n_complete_r3h0_to_make = 0
                    self.n_complete_r3h1_to_make = 0
                    self.n_complete_r3h2_to_make = 0
                    self.n_complete_r3h3_to_make = 0
                    self.n_complete_r4h0_to_make = 0
                    self.n_complete_r4h1_to_make = 0
                    self.n_complete_r5h0_to_make = 0
                    self.n_complete_r5h1_to_make = 0


                    if self.n_hybrids_to_make > 0:
                        if self.n_hybrids_to_make > leftover_production_capacity:
                            self.n_hybrids_to_make -= leftover_production_capacity
                            leftover_production_capacity = 0
                        else:
                            leftover_production_capacity -= self.n_hybrids_to_make
                            self.n_hybrids_to_make = 0

                    while ( leftover_production_capacity > 0.01 and (r0h0_capacity_reached == False or r0h1_capacity_reached == False or
                                                                        r1h0_capacity_reached == False or r1h1_capacity_reached == False or
                                                                        r2h0_capacity_reached == False or r3h0_capacity_reached == False or
                                                                        r3h1_capacity_reached == False or r3h2_capacity_reached == False or
                                                                        r3h3_capacity_reached == False or r4h0_capacity_reached == False or
                                                                        r4h1_capacity_reached == False or r5h0_capacity_reached == False or
                                                                        r5h1_capacity_reached == False)):


                        leftover_production_capacity = (actual_production_capacity - n_r0h0s_made -
                                                        n_r0h1s_made - n_r1h0s_made - n_r1h1s_made -
                                                        n_r2h0s_made - n_r3h0s_made - n_r3h1s_made -
                                                        n_r3h2s_made - n_r3h3s_made - n_r4h0s_made -
                                                        n_r4h1s_made - n_r5h0s_made - n_r5h1s_made )

                        total_n_hybrids_made = 0

                        if self.make_r0h0 == True:
                            total_n_hybrids_made += (self.total_complete_r0h0_made + n_r0h0s_made)/self.r0h0_production_weight
                        if self.make_r0h1 == True:
                            total_n_hybrids_made += (self.total_complete_r0h1_made + n_r0h1s_made)/self.r0h1_production_weight
                        if self.make_r1h0 == True:
                            total_n_hybrids_made += (self.total_complete_r1h0_made + n_r1h0s_made)/self.r1h0_production_weight
                        if self.make_r1h1 == True:
                            total_n_hybrids_made += (self.total_complete_r1h1_made + n_r1h1s_made)/self.r1h1_production_weight
                        if self.make_r2h0 == True:
                            total_n_hybrids_made += (self.total_complete_r2h0_made + n_r2h0s_made)/self.r2h0_production_weight
                        if self.make_r3h0 == True:
                            total_n_hybrids_made += (self.total_complete_r3h0_made + n_r3h0s_made)/self.r3h0_production_weight
                        if self.make_r3h1 == True:
                            total_n_hybrids_made += (self.total_complete_r3h1_made + n_r3h1s_made)/self.r3h1_production_weight
                        if self.make_r3h2 == True:
                            total_n_hybrids_made += (self.total_complete_r3h2_made + n_r3h2s_made)/self.r3h2_production_weight
                        if self.make_r3h3 == True:
                            total_n_hybrids_made += (self.total_complete_r3h3_made + n_r3h3s_made)/self.r3h3_production_weight
                        if self.make_r4h0 == True:
                            total_n_hybrids_made += (self.total_complete_r4h0_made + n_r4h0s_made)/self.r4h0_production_weight
                        if self.make_r4h1 == True:
                            total_n_hybrids_made += (self.total_complete_r4h1_made + n_r4h1s_made)/self.r4h1_production_weight
                        if self.make_r5h0 == True:
                            total_n_hybrids_made += (self.total_complete_r5h0_made + n_r5h0s_made)/self.r5h0_production_weight
                        if self.make_r5h1 == True:
                            total_n_hybrids_made += (self.total_complete_r5h1_made + n_r5h1s_made)/self.r5h1_production_weight


                        average_n_hybrids_made = total_n_hybrids_made/n_types_of_hybrids

                        # Make more of any hybrids that are below average in total number produced
                        while ( leftover_production_capacity > 0.01 
                               and (  ((self.total_complete_r0h0_made + n_r0h0s_made)/self.r0h0_production_weight < average_n_hybrids_made 
                                        and r0h0_capacity_reached == False)

                                    or ((self.total_complete_r0h1_made + n_r0h1s_made)/self.r0h1_production_weight < average_n_hybrids_made 
                                        and r0h1_capacity_reached == False)

                                    or ((self.total_complete_r1h0_made + n_r1h0s_made)/self.r1h0_production_weight < average_n_hybrids_made 
                                        and r1h0_capacity_reached == False)

                                    or ((self.total_complete_r1h1_made + n_r1h1s_made)/self.r1h1_production_weight < average_n_hybrids_made 
                                        and r1h1_capacity_reached == False)

                                    or ((self.total_complete_r2h0_made + n_r2h0s_made)/self.r2h0_production_weight < average_n_hybrids_made 
                                        and r2h0_capacity_reached == False)

                                    or ((self.total_complete_r3h0_made + n_r3h0s_made)/self.r3h0_production_weight < average_n_hybrids_made 
                                        and r3h0_capacity_reached == False)

                                    or ((self.total_complete_r3h1_made + n_r3h1s_made)/self.r3h1_production_weight < average_n_hybrids_made 
                                        and r3h1_capacity_reached == False)

                                    or ((self.total_complete_r3h2_made + n_r3h2s_made)/self.r3h2_production_weight < average_n_hybrids_made 
                                        and r3h2_capacity_reached == False)

                                    or ((self.total_complete_r3h3_made + n_r3h3s_made)/self.r3h3_production_weight < average_n_hybrids_made 
                                        and r3h3_capacity_reached == False)

                                    or ((self.total_complete_r4h0_made + n_r4h0s_made)/self.r4h0_production_weight < average_n_hybrids_made 
                                        and r4h0_capacity_reached == False)

                                    or ((self.total_complete_r4h1_made + n_r4h1s_made)/self.r4h1_production_weight < average_n_hybrids_made 
                                        and r4h1_capacity_reached == False)

                                    or ((self.total_complete_r5h0_made + n_r5h0s_made)/self.r5h0_production_weight < average_n_hybrids_made 
                                        and r5h0_capacity_reached == False)

                                    or ((self.total_complete_r5h1_made + n_r5h1s_made)/self.r5h1_production_weight < average_n_hybrids_made 
                                        and r5h1_capacity_reached == False))):

                            total_n_hybrids_made = 0

                            if self.make_r0h0 == True:
                                total_n_hybrids_made += (self.total_complete_r0h0_made + n_r0h0s_made)/self.r0h0_production_weight
                            if self.make_r0h1 == True:
                                total_n_hybrids_made += (self.total_complete_r0h1_made + n_r0h1s_made)/self.r0h1_production_weight
                            if self.make_r1h0 == True:
                                total_n_hybrids_made += (self.total_complete_r1h0_made + n_r1h0s_made)/self.r1h0_production_weight
                            if self.make_r1h1 == True:
                                total_n_hybrids_made += (self.total_complete_r1h1_made + n_r1h1s_made)/self.r1h1_production_weight
                            if self.make_r2h0 == True:
                                total_n_hybrids_made += (self.total_complete_r2h0_made + n_r2h0s_made)/self.r2h0_production_weight
                            if self.make_r3h0 == True:
                                total_n_hybrids_made += (self.total_complete_r3h0_made + n_r3h0s_made)/self.r3h0_production_weight
                            if self.make_r3h1 == True:
                                total_n_hybrids_made += (self.total_complete_r3h1_made + n_r3h1s_made)/self.r3h1_production_weight
                            if self.make_r3h2 == True:
                                total_n_hybrids_made += (self.total_complete_r3h2_made + n_r3h2s_made)/self.r3h2_production_weight
                            if self.make_r3h3 == True:
                                total_n_hybrids_made += (self.total_complete_r3h3_made + n_r3h3s_made)/self.r3h3_production_weight
                            if self.make_r4h0 == True:
                                total_n_hybrids_made += (self.total_complete_r4h0_made + n_r4h0s_made)/self.r4h0_production_weight
                            if self.make_r4h1 == True:
                                total_n_hybrids_made += (self.total_complete_r4h1_made + n_r4h1s_made)/self.r4h1_production_weight
                            if self.make_r5h0 == True:
                                total_n_hybrids_made += (self.total_complete_r5h0_made + n_r5h0s_made)/self.r5h0_production_weight
                            if self.make_r5h1 == True:
                                total_n_hybrids_made += (self.total_complete_r5h1_made + n_r5h1s_made)/self.r5h1_production_weight

                            average_n_hybrids_made = total_n_hybrids_made/n_types_of_hybrids

                            # Try to make one more R0H0
                            if r0h0_capacity_reached == False and (self.total_complete_r0h0_made + n_r0h0s_made)/self.r0h0_production_weight < average_n_hybrids_made:
                                if ( n_r0h0s_made + 1 > self.n_bare_r0h0.level or
                                     n_r0h0s_made + 1 > self.n_hccs.level or
                                     n_r0h0s_made + 1 > int(self.n_abcs.level/8) ):
                                    r0h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r0h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r0h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if abs(leftover_production_capacity) <= 0.01:
                                        break

                            # Try to make one more R0H1
                            if r0h1_capacity_reached == False and (self.total_complete_r0h1_made + n_r0h1s_made)/self.r0h1_production_weight < average_n_hybrids_made:
                                if ( n_r0h1s_made + 1 > self.n_bare_r0h1.level or
                                     n_r0h1s_made + 1 > self.n_hccs.level or
                                     n_r0h1s_made + 1 > int(self.n_abcs.level/9) ):
                                    r0h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r0h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r0h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R1H0
                            if r1h0_capacity_reached == False and (self.total_complete_r1h0_made + n_r1h0s_made)/self.r1h0_production_weight < average_n_hybrids_made:
                                if ( n_r1h0s_made + 1 > self.n_bare_r1h0.level or
                                     n_r1h0s_made + 1 > self.n_hccs.level or
                                     n_r1h0s_made + 1 > int(self.n_abcs.level/10) ):
                                    r1h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r1h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r1h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R1H1
                            if r1h1_capacity_reached == False and (self.total_complete_r1h1_made + n_r1h1s_made)/self.r1h1_production_weight < average_n_hybrids_made:
                                if ( n_r1h1s_made + 1 > self.n_bare_r1h1.level or
                                     n_r1h1s_made + 1 > self.n_hccs.level or
                                     n_r1h1s_made + 1 > int(self.n_abcs.level/11) ):
                                    r1h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r1h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r1h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R2H0
                            if r2h0_capacity_reached == False and (self.total_complete_r2h0_made + n_r2h0s_made)/self.r2h0_production_weight < average_n_hybrids_made:
                                if ( n_r2h0s_made + 1 > self.n_bare_r2h0.level or
                                     n_r2h0s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r2h0s_made + 1 > int(self.n_abcs.level/12) ):
                                    r2h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r2h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r2h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H0
                            if r3h0_capacity_reached == False and (self.total_complete_r3h0_made + n_r3h0s_made)/self.r3h0_production_weight < average_n_hybrids_made:
                                if ( n_r3h0s_made + 1 > self.n_bare_r3h0.level or
                                     n_r3h0s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H1
                            if r3h1_capacity_reached == False and (self.total_complete_r3h1_made + n_r3h1s_made)/self.r3h1_production_weight < average_n_hybrids_made:
                                if ( n_r3h1s_made + 1 > self.n_bare_r3h1.level or
                                     n_r3h1s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r3h1s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H2
                            if r3h2_capacity_reached == False and (self.total_complete_r3h2_made + n_r3h2s_made)/self.r3h2_production_weight < average_n_hybrids_made:
                                if ( n_r3h2s_made + 1 > self.n_bare_r3h2.level or
                                     n_r3h2s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h2_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h2s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h2s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H3
                            if r3h3_capacity_reached == False and (self.total_complete_r3h3_made + n_r3h3s_made)/self.r3h3_production_weight < average_n_hybrids_made:
                                if ( n_r3h3s_made + 1 > self.n_bare_r3h3.level or
                                     n_r3h3s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r3h3s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h3_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h3s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h3s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R4H0
                            if r4h0_capacity_reached == False and (self.total_complete_r4h0_made + n_r4h0s_made)/self.r4h0_production_weight < average_n_hybrids_made:
                                if ( n_r4h0s_made + 1 > self.n_bare_r4h0.level or
                                     n_r4h0s_made + 1 > int(self.n_abcs.level/8) ):
                                    r4h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r4h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r4h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R4H1
                            if r4h1_capacity_reached == False and (self.total_complete_r4h1_made + n_r4h1s_made)/self.r4h1_production_weight < average_n_hybrids_made:
                                if ( n_r4h1s_made + 1 > self.n_bare_r4h1.level or
                                     n_r4h1s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r4h1s_made + 1 > int(self.n_abcs.level/8) ):
                                    r4h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r4h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r4h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R5H0
                            if r5h0_capacity_reached == False and (self.total_complete_r5h0_made + n_r5h0s_made)/self.r5h0_production_weight < average_n_hybrids_made:
                                if ( n_r5h0s_made + 1 > self.n_bare_r5h0.level or
                                     n_r5h0s_made + 1 > int(self.n_abcs.level/9) ):
                                    r5h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r5h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r5h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R5H1
                            if r5h1_capacity_reached == False and (self.total_complete_r5h1_made + n_r5h1s_made)/self.r5h1_production_weight < average_n_hybrids_made:
                                if ( n_r5h1s_made + 1 > self.n_bare_r5h1.level or
                                     n_r5h1s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r5h1s_made + 1 > int(self.n_abcs.level/9) ):
                                    r5h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r5h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r5h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break


                        # If equal number of modules have been produced so far, then randomly select which ones to make one more of
                        random_list = list(range(0,13))
                        random.shuffle(random_list)

                        for itr in random_list:

                            # Try to make one more R0H0
                            if r0h0_capacity_reached == False and itr == 0:
                                if ( n_r0h0s_made + 1 > self.n_bare_r0h0.level or
                                     n_r0h0s_made + 1 > self.n_hccs.level or
                                     n_r0h0s_made + 1 > int(self.n_abcs.level/8) ):
                                    r0h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r0h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r0h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R0H1
                            if r0h1_capacity_reached == False and itr == 1:
                                if ( n_r0h1s_made + 1 > self.n_bare_r0h1.level or
                                     n_r0h1s_made + 1 > self.n_hccs.level or
                                     n_r0h1s_made + 1 > int(self.n_abcs.level/9) ):
                                    r0h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r0h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r0h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R1H0
                            if r1h0_capacity_reached == False and itr == 2:
                                if ( n_r1h0s_made + 1 > self.n_bare_r1h0.level or
                                     n_r1h0s_made + 1 > self.n_hccs.level or
                                     n_r1h0s_made + 1 > int(self.n_abcs.level/10) ):
                                    r1h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r1h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r1h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R1H1
                            if r1h1_capacity_reached == False and itr == 3:
                                if ( n_r1h1s_made + 1 > self.n_bare_r1h1.level or
                                     n_r1h1s_made + 1 > self.n_hccs.level or
                                     n_r1h1s_made + 1 > int(self.n_abcs.level/11) ):
                                    r1h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r1h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r1h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R2H0
                            if r2h0_capacity_reached == False and itr == 4:
                                if ( n_r2h0s_made + 1 > self.n_bare_r2h0.level or
                                     n_r2h0s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r2h0s_made + 1 > int(self.n_abcs.level/12) ):
                                    r2h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r2h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r2h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H0
                            if r3h0_capacity_reached == False and itr == 5:
                                if ( n_r3h0s_made + 1 > self.n_bare_r3h0.level or
                                     n_r3h0s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H1
                            if r3h1_capacity_reached == False and itr == 6:
                                if ( n_r3h1s_made + 1 > self.n_bare_r3h1.level or
                                     n_r3h1s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r3h1s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H2
                            if r3h2_capacity_reached == False and itr == 7:
                                if ( n_r3h2s_made + 1 > self.n_bare_r3h2.level or
                                     n_r3h2s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h2_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h2s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h2s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R3H3
                            if r3h3_capacity_reached == False and itr == 8:
                                if ( n_r3h3s_made + 1 > self.n_bare_r3h3.level or
                                     n_r3h3s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r3h3s_made + 1 > int(self.n_abcs.level/7) ):
                                    r3h3_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r3h3s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r3h3s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R4H0
                            if r4h0_capacity_reached == False and itr == 9:
                                if ( n_r4h0s_made + 1 > self.n_bare_r4h0.level or
                                     n_r4h0s_made + 1 > int(self.n_abcs.level/8) ):
                                    r4h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r4h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r4h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R4H1
                            if r4h1_capacity_reached == False and itr == 10:
                                if ( n_r4h1s_made + 1 > self.n_bare_r4h1.level or
                                     n_r4h1s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r4h1s_made + 1 > int(self.n_abcs.level/8) ):
                                    r4h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r4h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r4h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R5H0
                            if r5h0_capacity_reached == False and itr == 11:
                                if ( n_r5h0s_made + 1 > self.n_bare_r5h0.level or
                                     n_r5h0s_made + 1 > int(self.n_abcs.level/9) ):
                                    r5h0_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r5h0s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r5h0s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more R5H1
                            if r5h1_capacity_reached == False and itr == 12:
                                if ( n_r5h1s_made + 1 > self.n_bare_r5h1.level or
                                     n_r5h1s_made + 1 > int(self.n_hccs.level/2) or
                                     n_r5h1s_made + 1 > int(self.n_abcs.level/9) ):
                                    r5h1_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        n_r5h1s_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        n_r5h1s_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                    # Make the hybrids
                    if n_r0h0s_made > 0:
                        self.MakeR0H0(n_r0h0s_made, self.r0h0_yield)
                    if n_r0h1s_made > 0:
                        self.MakeR0H1(n_r0h1s_made, self.r0h1_yield)
                    if n_r1h0s_made > 0:
                        self.MakeR1H0(n_r1h0s_made, self.r1h0_yield)
                    if n_r1h1s_made > 0:
                        self.MakeR1H1(n_r1h1s_made, self.r1h1_yield)
                    if n_r2h0s_made > 0:
                        self.MakeR2H0(n_r2h0s_made, self.r2h0_yield)
                    if n_r3h0s_made > 0:
                        self.MakeR3H0(n_r3h0s_made, self.r3h0_yield)
                    if n_r3h1s_made > 0:
                        self.MakeR3H1(n_r3h1s_made, self.r3h1_yield)
                    if n_r3h2s_made > 0:
                        self.MakeR3H2(n_r3h2s_made, self.r3h2_yield)
                    if n_r3h3s_made > 0:
                        self.MakeR3H3(n_r3h3s_made, self.r3h3_yield)
                    if n_r4h0s_made > 0:
                        self.MakeR4H0(n_r4h0s_made, self.r4h0_yield)
                    if n_r4h1s_made > 0:
                        self.MakeR4H1(n_r4h1s_made, self.r4h1_yield)
                    if n_r5h0s_made > 0:
                        self.MakeR5H0(n_r5h0s_made, self.r5h0_yield)
                    if n_r5h1s_made > 0:
                        self.MakeR5H1(n_r5h1s_made, self.r5h1_yield)


            yield self.env.timeout(1)
                
    
    
    def FullInterrupt(self, process):
        
        while True:
            yield self.env.timeout( time_to_next_delay())
            process.interrupt()
    
    
    def HybridBurnIn(self, burn_in_capacity):
        
        while True:
            n_hybrids_to_burn_in = (self.n_pre_burn_in_r0h0.level + self.n_pre_burn_in_r0h1.level
                                    + self.n_pre_burn_in_r1h0.level + self.n_pre_burn_in_r1h1.level
                                    + self.n_pre_burn_in_r2h0.level + self.n_pre_burn_in_r3h0.level
                                    + self.n_pre_burn_in_r3h1.level + self.n_pre_burn_in_r3h2.level
                                    + self.n_pre_burn_in_r3h3.level + self.n_pre_burn_in_r4h0.level
                                    + self.n_pre_burn_in_r4h1.level + self.n_pre_burn_in_r5h0.level
                                    + self.n_pre_burn_in_r5h1.level)
            n_r0h0s_in_burn_in = self.n_pre_burn_in_r0h0.level
            n_r0h1s_in_burn_in = self.n_pre_burn_in_r0h1.level
            n_r1h0s_in_burn_in = self.n_pre_burn_in_r1h0.level
            n_r1h1s_in_burn_in = self.n_pre_burn_in_r1h1.level
            n_r2h0s_in_burn_in = self.n_pre_burn_in_r2h0.level
            n_r3h0s_in_burn_in = self.n_pre_burn_in_r3h0.level
            n_r3h1s_in_burn_in = self.n_pre_burn_in_r3h1.level
            n_r3h2s_in_burn_in = self.n_pre_burn_in_r3h2.level
            n_r3h3s_in_burn_in = self.n_pre_burn_in_r3h3.level
            n_r4h0s_in_burn_in = self.n_pre_burn_in_r4h0.level
            n_r4h1s_in_burn_in = self.n_pre_burn_in_r4h1.level
            n_r5h0s_in_burn_in = self.n_pre_burn_in_r5h0.level
            n_r5h1s_in_burn_in = self.n_pre_burn_in_r5h1.level
            
            if n_hybrids_to_burn_in == 0:
                yield self.env.timeout(7)
                
            if n_hybrids_to_burn_in > burn_in_capacity:
                
                while n_hybrids_to_burn_in > burn_in_capacity:
                    
                    if n_r0h0s_in_burn_in > 0:
                        n_r0h0s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r0h1s_in_burn_in > 0:
                        n_r0h1s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r1h0s_in_burn_in > 0:
                        n_r1h0s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r1h1s_in_burn_in > 0:
                        n_r1h1s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r2h0s_in_burn_in > 0:
                        n_r2h0s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r3h0s_in_burn_in > 0:
                        n_r3h0s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r3h1s_in_burn_in > 0:
                        n_r3h1s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r3h2s_in_burn_in > 0:
                        n_r3h2s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r3h3s_in_burn_in > 0:
                        n_r3h3s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r4h0s_in_burn_in > 0:
                        n_r4h0s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r4h1s_in_burn_in > 0:
                        n_r4h1s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r5h0s_in_burn_in > 0:
                        n_r5h0s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_r5h1s_in_burn_in > 0:
                        n_r5h1s_in_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                                    
        
                
            if n_r0h0s_in_burn_in > 0:
                self.n_pre_burn_in_r0h0.get(n_r0h0s_in_burn_in)
            if n_r0h1s_in_burn_in > 0:
                self.n_pre_burn_in_r0h1.get(n_r0h1s_in_burn_in)
            if n_r1h0s_in_burn_in > 0:
                self.n_pre_burn_in_r1h0.get(n_r1h0s_in_burn_in)
            if n_r1h1s_in_burn_in > 0:
                self.n_pre_burn_in_r1h1.get(n_r1h1s_in_burn_in)
            if n_r2h0s_in_burn_in > 0:
                self.n_pre_burn_in_r2h0.get(n_r2h0s_in_burn_in)
            if n_r3h0s_in_burn_in > 0:
                self.n_pre_burn_in_r3h0.get(n_r3h0s_in_burn_in)
            if n_r3h1s_in_burn_in > 0:
                self.n_pre_burn_in_r3h1.get(n_r3h1s_in_burn_in)
            if n_r3h2s_in_burn_in > 0:
                self.n_pre_burn_in_r3h2.get(n_r3h2s_in_burn_in)
            if n_r3h3s_in_burn_in > 0:
                self.n_pre_burn_in_r3h3.get(n_r3h3s_in_burn_in)
            if n_r4h0s_in_burn_in > 0:
                self.n_pre_burn_in_r4h0.get(n_r4h0s_in_burn_in)
            if n_r4h1s_in_burn_in > 0:
                self.n_pre_burn_in_r4h1.get(n_r4h1s_in_burn_in)
            if n_r5h0s_in_burn_in > 0:
                self.n_pre_burn_in_r5h0.get(n_r5h0s_in_burn_in)
            if n_r5h1s_in_burn_in > 0:
                self.n_pre_burn_in_r5h1.get(n_r5h1s_in_burn_in)
                    
            

            yield self.env.timeout(7)

            for n in range(n_r0h0s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r0h0.put(1)
                    self.n_complete_r0h0_made_today += 1
            for n in range(n_r0h1s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r0h1.put(1)
                    self.n_complete_r0h1_made_today += 1
            for n in range(n_r1h0s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r1h0.put(1)
                    self.n_complete_r1h0_made_today += 1
            for n in range(n_r1h1s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r1h1.put(1)
                    self.n_complete_r1h1_made_today += 1
            for n in range(n_r2h0s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r2h0.put(1)
                    self.n_complete_r2h0_made_today += 1
            for n in range(n_r3h0s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r3h0.put(1)
                    self.n_complete_r3h0_made_today += 1
            for n in range(n_r3h1s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r3h1.put(1)
                    self.n_complete_r3h1_made_today += 1
            for n in range(n_r3h2s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r3h2.put(1)
                    self.n_complete_r3h2_made_today += 1
            for n in range(n_r3h3s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r3h3.put(1)
                    self.n_complete_r3h3_made_today += 1
            for n in range(n_r4h0s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r4h0.put(1)
                    self.n_complete_r4h0_made_today += 1
            for n in range(n_r4h1s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r4h1.put(1)
                    self.n_complete_r4h1_made_today += 1
            for n in range(n_r5h0s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r5h0.put(1)
                    self.n_complete_r5h0_made_today += 1
            for n in range(n_r5h1s_in_burn_in):
                if random.uniform(0,1) < self.hybrid_burn_in_yield:
                    self.n_complete_r5h1.put(1)
                    self.n_complete_r5h1_made_today += 1
        
        
        
    def MakeR0H0(self, n_hybrids_made, yield_fraction):
        
        n_items_made = 0

        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(8*n_items_made)
            self.n_hccs.get(n_items_made)
            self.n_bare_r0h0.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r0h0.put(n_items_made)
                self.total_complete_r0h0_made += n_items_made

            
    def MakeR0H1(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(9*n_items_made)
            self.n_hccs.get(n_items_made)
            self.n_bare_r0h1.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r0h1.put(n_items_made)
                self.total_complete_r0h1_made += n_items_made

            
    def MakeR1H0(self, n_hybrids_made, yield_fraction):

        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(10*n_items_made)
            self.n_hccs.get(n_items_made)
            self.n_bare_r1h0.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r1h0.put(n_items_made)
                self.total_complete_r1h0_made += n_items_made

            
    def MakeR1H1(self, n_hybrids_made, yield_fraction):       

        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(11*n_items_made)
            self.n_hccs.get(n_items_made)
            self.n_bare_r1h1.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r1h1.put(n_items_made)
                self.total_complete_r1h1_made += n_items_made

            
    def MakeR2H0(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(12*n_items_made)
            self.n_hccs.get(2*n_items_made)
            self.n_bare_r2h0.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r2h0.put(n_items_made)
                self.total_complete_r2h0_made += n_items_made

            
    def MakeR3H0(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(7*n_items_made)
            self.n_bare_r3h0.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r3h0.put(n_items_made)
                self.total_complete_r3h0_made += n_items_made

            
    def MakeR3H1(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(7*n_items_made)
            self.n_hccs.get(2*n_items_made)
            self.n_bare_r3h1.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r3h1.put(n_items_made)
                self.total_complete_r3h1_made += n_items_made

            
    def MakeR3H2(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(7*n_items_made)
            self.n_bare_r3h2.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r3h2.put(n_items_made)
                self.total_complete_r3h2_made += n_items_made

            
    def MakeR3H3(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(7*n_items_made)
            self.n_hccs.get(2*n_items_made)
            self.n_bare_r3h3.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r3h3.put(n_items_made)
                self.total_complete_r3h3_made += n_items_made

                    
    def MakeR4H0(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(8*n_items_made)
            self.n_bare_r4h0.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r4h0.put(n_items_made)
                self.total_complete_r4h0_made += n_items_made

            
    def MakeR4H1(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(8*n_items_made)
            self.n_hccs.get(2*n_items_made)
            self.n_bare_r4h1.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r4h1.put(n_items_made)
                self.total_complete_r4h1_made += n_items_made


    def MakeR5H0(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(9*n_items_made)
            self.n_bare_r5h0.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r5h0.put(n_items_made)
                self.total_complete_r5h0_made += n_items_made

            
    def MakeR5H1(self, n_hybrids_made, yield_fraction):       
        
        n_items_made = 0
        
        if n_hybrids_made - math.floor(n_hybrids_made) > 0.01:
            if random.uniform(0,1) <= n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.ceil(n_hybrids_made)))
            elif random.uniform(0,1) > n_hybrids_made - math.floor(n_hybrids_made):
                n_items_made = int(round(math.floor(n_hybrids_made)))
        else:
            n_items_made = int(round(n_hybrids_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(9*n_items_made)
            self.n_hccs.get(2*n_items_made)
            self.n_bare_r5h1.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_r5h1.put(n_items_made)
                self.total_complete_r5h1_made += n_items_made
                

                
    def MakeR0Modules(self, n_modules_made, yield_fraction):
        
        n_items_made = n_modules_made
        
        if self.total_r0_modules_made + n_items_made - math.ceil(self.total_r0_modules_made) > 0:   
            # Make items
            for i in range(math.floor(self.total_r0_modules_made), math.floor(self.total_r0_modules_made + n_items_made)):
                self.n_r0_sensors.get(1)
                self.n_complete_r0h0.get(1)
                #self.n_hybrid_glues.get(1)
                self.n_complete_r0h1.get(1)
                #self.n_hybrid_glues.get(1)
                self.n_r0_powerboards.get(1)
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
                    
        if n_items_made < 0:
            self.n_r0_modules.get(abs(n_items_made))
            self.n_r0_modules_made_today += n_items_made
            self.total_r0_modules_made += n_items_made
        elif n_items_made > 0:
            self.n_r0_modules.put(n_items_made)
            self.n_r0_modules_made_today += n_items_made
            self.total_r0_modules_made += n_items_made

            
    def MakeR1Modules(self, n_modules_made, yield_fraction):
        
        n_items_made = n_modules_made
        
        if self.total_r1_modules_made + n_items_made - math.ceil(self.total_r1_modules_made) > 0:   
            # Make items
            for i in range(math.floor(self.total_r1_modules_made), math.floor(self.total_r1_modules_made + n_items_made)):
                self.n_r1_sensors.get(1)
                self.n_complete_r1h0.get(1)
                #self.n_hybrid_glues.get(1)
                self.n_complete_r1h1.get(1)
                #self.n_hybrid_glues.get(1)
                self.n_r1_powerboards.get(1)
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
                    
        if n_items_made < 0:
            self.n_r1_modules.get(abs(n_items_made))
            self.n_r1_modules_made_today += n_items_made
            self.total_r1_modules_made += n_items_made
        elif n_items_made > 0:
            self.n_r1_modules.put(n_items_made)
            self.n_r1_modules_made_today += n_items_made
            self.total_r1_modules_made += n_items_made

            
    def MakeR2Modules(self, n_modules_made, yield_fraction):
        
        n_items_made = n_modules_made
        
        if self.total_r2_modules_made + n_items_made - math.ceil(self.total_r2_modules_made) > 0:   
            # Make items
            for i in range(math.floor(self.total_r2_modules_made), math.floor(self.total_r2_modules_made + n_items_made)):
                self.n_r2_sensors.get(1)
                self.n_complete_r2h0.get(1)
                #self.n_hybrid_glues.get(1)
                self.n_r2_powerboards.get(1)
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 1
                    
        if n_items_made < 0:
            self.n_r2_modules.get(abs(n_items_made))
            self.n_r2_modules_made_today += n_items_made
            self.total_r2_modules_made += n_items_made
        elif n_items_made > 0:
            self.n_r2_modules.put(n_items_made)
            self.n_r2_modules_made_today += n_items_made
            self.total_r2_modules_made += n_items_made

            
    def MakeR3Modules(self, n_modules_made, yield_fraction):
        
        n_items_made = n_modules_made
        # Make items
        if (self.total_r3_modules_made + n_items_made)/2 - math.floor(self.total_r3_modules_made)//2 + 1 > 0:   
            # Make items
            for i in range(math.floor(self.total_r3_modules_made)//2, math.floor(self.total_r3_modules_made + n_items_made)//2):
                self.n_r3_sensors.get(2)
                self.n_complete_r3h0.get(1)
                self.n_complete_r3h1.get(1)
                self.n_complete_r3h2.get(1)
                self.n_complete_r3h3.get(1)
                self.n_r3_powerboards.get(1)
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 2
                    
        if n_items_made < 0:
            self.n_r3_modules.get(abs(n_items_made))
            self.n_r3_modules_made_today += n_items_made
            self.total_r3_modules_made += n_items_made
        elif n_items_made > 0:
            self.n_r3_modules.put(n_items_made)
            self.n_r3_modules_made_today += n_items_made
            self.total_r3_modules_made += n_items_made

            
    def MakeR4Modules(self, n_modules_made, yield_fraction):
        
        n_items_made = n_modules_made
        # Make items
        if (self.total_r4_modules_made + n_items_made)/2 - math.floor(self.total_r4_modules_made)//2 + 1 > 0:            
            # Make items
            for i in range(math.floor(self.total_r4_modules_made)//2, math.floor(self.total_r4_modules_made + n_items_made)//2):
                self.n_r4_sensors.get(2)
                self.n_complete_r4h0.get(1)
                self.n_complete_r4h1.get(1)
                self.n_r4_powerboards.get(1)
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 2
                    
        if n_items_made < 0:
            self.n_r4_modules.get(abs(n_items_made))
            self.n_r4_modules_made_today += n_items_made
            self.total_r4_modules_made += n_items_made
        elif n_items_made > 0:
            self.n_r4_modules.put(n_items_made)
            self.n_r4_modules_made_today += n_items_made
            self.total_r4_modules_made += n_items_made

        
    def MakeR5Modules(self, n_modules_made, yield_fraction):
        
        n_items_made = n_modules_made
        # Make items
        if (self.total_r5_modules_made + n_items_made)/2 - math.floor(self.total_r5_modules_made)//2 + 1 > 0:            
            # Make items
            for i in range(math.floor(self.total_r5_modules_made)//2, math.floor(self.total_r5_modules_made + n_items_made)//2):
                self.n_r5_sensors.get(2)
                self.n_complete_r5h0.get(1)
                self.n_complete_r5h1.get(1)
                self.n_r5_powerboards.get(1)
                if random.uniform(0,1) > yield_fraction:
                    n_items_made -= 2
                    
        if n_items_made < 0:
            self.n_r5_modules.get(abs(n_items_made))
            self.n_r5_modules_made_today += n_items_made
            self.total_r5_modules_made += n_items_made
        elif n_items_made > 0:
            self.n_r5_modules.put(n_items_made)
            self.n_r5_modules_made_today += n_items_made
            self.total_r5_modules_made += n_items_made

                    

                    
    def MakePetal(self, days_to_make_petal, std, yield_fraction):
        
        while True:
            # Don't work if it's the weekend
            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)
                
            else: 
                if (self.n_r0_modules.level >= 2 and self.n_r1_modules.level >= 2 and self.n_r2_modules.level >= 2
                    and self.n_r3_modules.level >= 4 and self.n_r4_modules.level >= 4 and self.n_r5_modules.level >= 4):
                    self.n_r0_modules.get(2)
                    self.n_r1_modules.get(2)
                    self.n_r2_modules.get(2)
                    self.n_r3_modules.get(4)
                    self.n_r4_modules.get(4)
                    self.n_r5_modules.get(4)
                    if random.uniform(0,1) > yield_fraction:
                        yield self.env.timeout(1)
                    else:
                        self.n_petals.put(1)
                        yield self.env.timeout(7)
                else:
                    yield self.env.timeout(1)
                        
    def GetNumberOfHybridTypes(self):
        
        n_types_of_hybrids = 0
        
        if self.make_r0h0 == True:
            n_types_of_hybrids += 1
        if self.make_r0h1 == True:
            n_types_of_hybrids += 1
        if self.make_r1h0 == True:
            n_types_of_hybrids += 1
        if self.make_r1h1 == True:
            n_types_of_hybrids += 1
        if self.make_r2h0 == True:
            n_types_of_hybrids += 1
        if self.make_r3h0 == True:
            n_types_of_hybrids += 1
        if self.make_r3h1 == True:
            n_types_of_hybrids += 1
        if self.make_r3h2 == True:
            n_types_of_hybrids += 1
        if self.make_r3h3 == True:
            n_types_of_hybrids += 1
        if self.make_r4h0 == True:
            n_types_of_hybrids += 1
        if self.make_r4h1 == True:
            n_types_of_hybrids += 1
        if self.make_r5h0 == True:
            n_types_of_hybrids += 1
        if self.make_r5h1 == True:
            n_types_of_hybrids += 1
            
        return n_types_of_hybrids
    
    def GetNumberOfModuleTypes(self):
        
        n_types_of_modules = 0

        if self.make_r0_modules == True:
            n_types_of_modules += 1
        if self.make_r1_modules == True:
            n_types_of_modules += 1
        if self.make_r2_modules == True:
            n_types_of_modules += 1
        if self.make_r3_modules == True:
            n_types_of_modules += 1
        if self.make_r4_modules == True:
            n_types_of_modules += 1
        if self.make_r5_modules == True:
            n_types_of_modules += 1
            
        return n_types_of_modules
        
    def ClearItemsMade(self):
        while True:
            self.n_complete_r0h0_made_today = 0
            self.n_complete_r0h1_made_today = 0
            self.n_complete_r1h0_made_today = 0
            self.n_complete_r1h1_made_today = 0
            self.n_complete_r2h0_made_today = 0
            self.n_complete_r3h0_made_today = 0
            self.n_complete_r3h1_made_today = 0
            self.n_complete_r3h2_made_today = 0
            self.n_complete_r3h3_made_today = 0
            self.n_complete_r4h0_made_today = 0
            self.n_complete_r4h1_made_today = 0
            self.n_complete_r5h0_made_today = 0
            self.n_complete_r5h1_made_today = 0
            self.n_r0_modules_made_today = 0
            self.n_r1_modules_made_today = 0
            self.n_r2_modules_made_today = 0
            self.n_r3_modules_made_today = 0
            self.n_r4_modules_made_today = 0
            self.n_r5_modules_made_today = 0
            yield self.env.timeout(1)

            
class SensorTestSite(object):
    
    def __init__(self, env, name):
        self.env = env
        self.name = name
        
        self.rest_until_date = 0

        self.test_rate = 0.0
        self.test_yield = 1.0
        
        self.test_r0 = True
        self.test_r1 = True
        self.test_r2 = True
        self.test_r3 = True
        self.test_r4 = True
        self.test_r5 = True
            
        self.n_untested_r0_sensors = simpy.Container(env)
        self.n_untested_r1_sensors = simpy.Container(env)
        self.n_untested_r2_sensors = simpy.Container(env)
        self.n_untested_r3_sensors = simpy.Container(env)
        self.n_untested_r4_sensors = simpy.Container(env)
        self.n_untested_r5_sensors = simpy.Container(env)
        
        self.n_r0_sensors = simpy.Container(env)
        self.n_r1_sensors = simpy.Container(env)
        self.n_r2_sensors = simpy.Container(env)
        self.n_r3_sensors = simpy.Container(env)
        self.n_r4_sensors = simpy.Container(env)
        self.n_r5_sensors = simpy.Container(env)
        
        self.n_abcs = simpy.Container(env)
        self.n_hccs = simpy.Container(env)
        
        # tracking
        
        self.total_r0_sensors_made = 0
        self.total_r1_sensors_made = 0
        self.total_r2_sensors_made = 0
        self.total_r3_sensors_made = 0
        self.total_r4_sensors_made = 0
        self.total_r5_sensors_made = 0
        
        # shipping
        
        self.n_r0_sensors_made_today = 0
        self.n_r1_sensors_made_today = 0
        self.n_r2_sensors_made_today = 0
        self.n_r3_sensors_made_today = 0
        self.n_r4_sensors_made_today = 0
        self.n_r5_sensors_made_today = 0
        
        self.n_abcs_made_today = 0
        self.n_hccs_made_today = 0
        
        self.n_r0_sensors_to_make = 0
        self.n_r1_sensors_to_make = 0
        self.n_r2_sensors_to_make = 0
        self.n_r3_sensors_to_make = 0
        self.n_r4_sensors_to_make = 0
        self.n_r5_sensors_to_make = 0
        self.n_sensors_to_make = 0
        
        self.n_abcs_to_make = 0
        self.n_hccs_to_make = 0
        
    def ScheduleSensorReception(self, arrival_date, n_r0_received, n_r1_received, n_r2_received, n_r3_received, n_r4_received, n_r5_received):
               
            yield self.env.timeout(arrival_date)
            if n_r0_received > 0:
                self.n_untested_r0_sensors.put(n_r0_received)
            if n_r1_received > 0:
                self.n_untested_r1_sensors.put(n_r1_received)
            if n_r2_received > 0:
                self.n_untested_r2_sensors.put(n_r2_received)
            if n_r3_received > 0:
                self.n_untested_r3_sensors.put(n_r3_received)
            if n_r4_received > 0:
                self.n_untested_r4_sensors.put(n_r4_received)
            if n_r5_received > 0:
                self.n_untested_r5_sensors.put(n_r5_received)
                      
            
                    
    def ReadSensorDeliverySchedule(self, input_csv):
            
        with open(input_csv) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')

            line = 0

            for row in csv_reader:

                if line == 0:
                    line += 1
                    continue
                        
                arrival_date = int(row[0])
                n_r0_received = float(row[1])
                n_r1_received = float(row[2])
                n_r2_received = float(row[3])
                n_r3_received = float(row[4])
                n_r4_received = float(row[5])
                n_r5_received = float(row[6])

                self.env.process(self.ScheduleSensorReception(arrival_date, n_r0_received, n_r1_received, n_r2_received, n_r3_received, n_r4_received, n_r5_received))
        
    def TestSensors(self):
            
        while True:
                
            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)
                
            else:
                
                if random.uniform(0,1) > self.test_rate - math.floor(self.test_rate):
                    n_sensors_to_test = math.floor(self.test_rate)
                else:
                    n_sensors_to_test = math.ceil(self.test_rate)

                n_sensor_types = 0

                if self.test_r0 == True:
                    n_sensor_types += 1
                if self.test_r1 == True:
                    n_sensor_types += 1
                if self.test_r2 == True:
                    n_sensor_types += 1
                if self.test_r3 == True:
                    n_sensor_types += 2
                if self.test_r4 == True:
                    n_sensor_types += 2
                if self.test_r5 == True:
                    n_sensor_types += 2
                    
                self.n_sensors_to_make += (self.n_r0_sensors_to_make + self.n_r1_sensors_to_make + self.n_r2_sensors_to_make 
                                                + self.n_r3_sensors_to_make + self.n_r4_sensors_to_make + self.n_r5_sensors_to_make)
                    
                self.n_r0_sensors_to_make = 0
                self.n_r1_sensors_to_make = 0
                self.n_r2_sensors_to_make = 0
                self.n_r3_sensors_to_make = 0
                self.n_r4_sensors_to_make = 0
                self.n_r5_sensors_to_make = 0

                if self.n_sensors_to_make > n_sensors_to_test:
                    self.n_sensors_to_make -= n_sensors_to_test
                    n_sensors_to_test = 0
                else:
                    n_sensors_to_test -= self.n_sensors_to_make
                    self.n_sensors_to_make = 0


                while n_sensors_to_test > 0 and (self.n_untested_r0_sensors.level > 0 or self.n_untested_r1_sensors.level > 0
                                                    or self.n_untested_r2_sensors.level > 0 or self.n_untested_r3_sensors.level > 0
                                                    or self.n_untested_r4_sensors.level > 0 or self.n_untested_r5_sensors.level > 0):

                    total_sensors_made = (self.total_r0_sensors_made + self.total_r1_sensors_made + self.total_r2_sensors_made
                                            + self.total_r3_sensors_made + self.total_r4_sensors_made + self.total_r5_sensors_made)

                    average_sensors_made = total_sensors_made / n_sensor_types

                    while ( (self.total_r0_sensors_made < average_sensors_made and self.n_untested_r0_sensors.level > 0)
                                or (self.total_r1_sensors_made < average_sensors_made and self.n_untested_r1_sensors.level > 0)
                                or (self.total_r2_sensors_made < average_sensors_made and self.n_untested_r2_sensors.level > 0)
                                or (self.total_r3_sensors_made < average_sensors_made*2 and self.n_untested_r3_sensors.level > 0)
                                or (self.total_r4_sensors_made < average_sensors_made*2 and self.n_untested_r4_sensors.level > 0)
                                or (self.total_r5_sensors_made < average_sensors_made*2 and self.n_untested_r5_sensors.level > 0) ):

                        total_sensors_made = (self.total_r0_sensors_made + self.total_r1_sensors_made + self.total_r2_sensors_made
                                            + self.total_r3_sensors_made + self.total_r4_sensors_made + self.total_r5_sensors_made)

                        average_sensors_made = total_sensors_made / n_sensor_types


                        if self.test_r0 == True and self.total_r0_sensors_made < average_sensors_made and self.n_untested_r0_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r0_sensors.get(1)
                            self.total_r0_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r0_sensors.put(1)
                                self.n_r0_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r1 == True and self.total_r1_sensors_made < average_sensors_made and self.n_untested_r1_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r1_sensors.get(1)
                            self.total_r1_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r1_sensors.put(1)
                                self.n_r1_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r2 == True and self.total_r2_sensors_made < average_sensors_made and self.n_untested_r2_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r2_sensors.get(1)
                            self.total_r2_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r2_sensors.put(1)
                                self.n_r2_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r3 == True and self.total_r3_sensors_made < average_sensors_made*2 and self.n_untested_r3_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r3_sensors.get(1)
                            self.total_r3_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r3_sensors.put(1)
                                self.n_r3_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r4 == True and self.total_r4_sensors_made < average_sensors_made*2 and self.n_untested_r4_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r4_sensors.get(1)
                            self.total_r4_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r4_sensors.put(1)
                                self.n_r4_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r5 == True and self.total_r5_sensors_made < average_sensors_made*2 and self.n_untested_r5_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r5_sensors.get(1)
                            self.total_r5_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r5_sensors.put(1)
                                self.n_r5_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break

                    if n_sensors_to_test == 0:
                        break


                    random_list = list(range(0,6))
                    random.shuffle(random_list)

                    for itr in random_list:

                        if self.test_r0 == True and itr == 0 and self.n_untested_r0_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r0_sensors.get(1)
                            self.total_r0_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r0_sensors.put(1)
                                self.n_r0_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r1 == True and itr == 1 and self.n_untested_r1_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r1_sensors.get(1)
                            self.total_r1_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r1_sensors.put(1)
                                self.n_r1_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r2 == True and itr == 2 and self.n_untested_r2_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r2_sensors.get(1)
                            self.total_r2_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r2_sensors.put(1)
                                self.n_r2_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r3 == True and itr == 3 and self.n_untested_r3_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r3_sensors.get(1)
                            self.total_r3_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r3_sensors.put(1)
                                self.n_r3_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r4 == True and itr == 4 and self.n_untested_r4_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r4_sensors.get(1)
                            self.total_r4_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r4_sensors.put(1)
                                self.n_r4_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break
                        if self.test_r5 == True and itr == 5 and self.n_untested_r5_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_r5_sensors.get(1)
                            self.total_r5_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_r5_sensors.put(1)
                                self.n_r5_sensors_made_today += 1
                            if n_sensors_to_test == 0:
                                break

                yield self.env.timeout(1)
                
        

                
    def TestABCs(self, daily_rate, abc_test_yield):

        while True:

            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)

            for i in range(daily_rate - self.n_abcs_to_make):
                if random.uniform(0,1) < abc_test_yield:
                    self.n_abcs.put(1)
                    self.n_abcs_made_today += 1
                    
            self.n_abcs_to_make = 0
                        
            yield self.env.timeout(1)

    def ClearItemsMade(self):
        while True:
            self.n_r0_sensors_made_today = 0
            self.n_r1_sensors_made_today = 0
            self.n_r2_sensors_made_today = 0
            self.n_r3_sensors_made_today = 0
            self.n_r4_sensors_made_today = 0
            self.n_r5_sensors_made_today = 0

            self.n_abs_made_today = 0
            self.n_hccs_made_today = 0

            yield self.env.timeout(1)
        
            
class RawPartMaker(object):
    
    def __init__(self, env, name):
        self.env = env
        self.name = name
        
        self.rest_until_date = 0
        
        self.n_bare_r0h0 = simpy.Container(env)
        self.n_bare_r0h1 = simpy.Container(env)
        self.n_bare_r1h0 = simpy.Container(env)
        self.n_bare_r1h1 = simpy.Container(env)
        self.n_bare_r2h0 = simpy.Container(env)
        self.n_bare_r3h0 = simpy.Container(env)
        self.n_bare_r3h1 = simpy.Container(env)
        self.n_bare_r3h2 = simpy.Container(env)
        self.n_bare_r3h3 = simpy.Container(env)
        self.n_bare_r4h0 = simpy.Container(env)
        self.n_bare_r4h1 = simpy.Container(env)
        self.n_bare_r5h0 = simpy.Container(env)
        self.n_bare_r5h1 = simpy.Container(env)
        
        self.n_r0_powerboards = simpy.Container(env)
        self.n_r1_powerboards = simpy.Container(env)
        self.n_r2_powerboards = simpy.Container(env)
        self.n_r3_powerboards = simpy.Container(env)
        self.n_r4_powerboards = simpy.Container(env)
        self.n_r5_powerboards = simpy.Container(env)
        
        # tracking stuff
        
        self.total_bare_r0h0_made = 0
        self.total_bare_r0h1_made = 0
        self.total_bare_r1h0_made = 0
        self.total_bare_r1h1_made = 0
        self.total_bare_r2h0_made = 0
        self.total_bare_r3h0_made = 0
        self.total_bare_r3h1_made = 0
        self.total_bare_r3h2_made = 0
        self.total_bare_r3h3_made = 0
        self.total_bare_r4h0_made = 0
        self.total_bare_r4h1_made = 0
        self.total_bare_r5h0_made = 0
        self.total_bare_r5h1_made = 0
        
        self.total_r0_powerboards_made = 0
        self.total_r1_powerboards_made = 0
        self.total_r2_powerboards_made = 0
        self.total_r3_powerboards_made = 0
        self.total_r4_powerboards_made = 0
        self.total_r5_powerboards_made = 0

        
        self.n_bare_r0h0_made_today = 0
        self.n_bare_r0h1_made_today = 0
        self.n_bare_r1h0_made_today = 0
        self.n_bare_r1h1_made_today = 0
        self.n_bare_r2h0_made_today = 0
        self.n_bare_r3h0_made_today = 0
        self.n_bare_r3h1_made_today = 0
        self.n_bare_r3h2_made_today = 0
        self.n_bare_r3h3_made_today = 0
        self.n_bare_r4h0_made_today = 0
        self.n_bare_r4h1_made_today = 0
        self.n_bare_r5h0_made_today = 0
        self.n_bare_r5h1_made_today = 0
        
        self.n_r0_powerboards_made_today = 0
        self.n_r1_powerboards_made_today = 0
        self.n_r2_powerboards_made_today = 0
        self.n_r3_powerboards_made_today = 0
        self.n_r4_powerboards_made_today = 0
        self.n_r5_powerboards_made_today = 0
        
        self.n_bare_r0h0_to_make = 0
        self.n_bare_r0h1_to_make = 0
        self.n_bare_r1h0_to_make = 0
        self.n_bare_r1h1_to_make = 0
        self.n_bare_r2h0_to_make = 0
        self.n_bare_r3h0_to_make = 0
        self.n_bare_r3h1_to_make = 0
        self.n_bare_r3h2_to_make = 0
        self.n_bare_r3h3_to_make = 0
        self.n_bare_r4h0_to_make = 0
        self.n_bare_r4h1_to_make = 0
        self.n_bare_r5h0_to_make = 0
        self.n_bare_r5h1_to_make = 0
        
        self.n_r0_powerboards_to_make = 0
        self.n_r1_powerboards_to_make = 0
        self.n_r2_powerboards_to_make = 0
        self.n_r3_powerboards_to_make = 0
        self.n_r4_powerboards_to_make = 0
        self.n_r5_powerboards_to_make = 0
        
        

    def MakeBareHybrids(self, r0h0_rate, r0h1_rate, r1h0_rate, r1h1_rate, r2h0_rate, r3h0_rate,
                           r3h1_rate, r3h2_rate, r3h3_rate, r4h0_rate, r4h1_rate, r5h0_rate, r5h1_rate):
        
        while True:
            
            try:
            
                if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                    yield self.env.timeout(1)

                else:

                    if self.n_bare_r0h0_to_make > r0h0_rate:
                        self.n_bare_r0h0_to_make -= r0h0_rate
                    else:
                        self.n_bare_r0h0.put(r0h0_rate - self.n_bare_r0h0_to_make)
                        self.n_bare_r0h0_made_today += r0h0_rate - self.n_bare_r0h0_to_make
                        self.n_bare_r0h0_to_make = 0

                    if self.n_bare_r0h1_to_make > r0h1_rate:
                        self.n_bare_r0h1_to_make -= r0h1_rate
                    else:
                        self.n_bare_r0h1.put(r0h1_rate - self.n_bare_r0h1_to_make)
                        self.n_bare_r0h1_made_today += r0h1_rate - self.n_bare_r0h1_to_make
                        self.n_bare_r0h1_to_make = 0

                    if self.n_bare_r1h0_to_make > r1h0_rate:
                        self.n_bare_r1h0_to_make -= r1h0_rate
                    else:
                        self.n_bare_r1h0.put(r1h0_rate - self.n_bare_r1h0_to_make)
                        self.n_bare_r1h0_made_today += r1h0_rate - self.n_bare_r1h0_to_make
                        self.n_bare_r1h0_to_make = 0

                    if self.n_bare_r1h1_to_make > r1h1_rate:
                        self.n_bare_r1h1_to_make -= r1h1_rate
                    else:
                        self.n_bare_r1h1.put(r1h1_rate - self.n_bare_r1h1_to_make)
                        self.n_bare_r1h1_made_today += r1h1_rate - self.n_bare_r1h1_to_make
                        self.n_bare_r1h1_to_make = 0

                    if self.n_bare_r2h0_to_make > r2h0_rate:
                        self.n_bare_r2h0_to_make -= r2h0_rate
                    else:
                        self.n_bare_r2h0.put(r2h0_rate - self.n_bare_r2h0_to_make)
                        self.n_bare_r2h0_made_today += r2h0_rate - self.n_bare_r2h0_to_make
                        self.n_bare_r2h0_to_make = 0

                    if self.n_bare_r3h0_to_make > r3h0_rate:
                        self.n_bare_r3h0_to_make -= r3h0_rate
                    else:
                        self.n_bare_r3h0.put(r3h0_rate - self.n_bare_r3h0_to_make)
                        self.n_bare_r3h0_made_today += r3h0_rate - self.n_bare_r3h0_to_make
                        self.n_bare_r3h0_to_make = 0

                    if self.n_bare_r3h1_to_make > r3h1_rate:
                        self.n_bare_r3h1_to_make -= r3h1_rate
                    else:
                        self.n_bare_r3h1.put(r3h1_rate - self.n_bare_r3h1_to_make)
                        self.n_bare_r3h1_made_today += r3h1_rate - self.n_bare_r3h1_to_make
                        self.n_bare_r3h1_to_make = 0

                    if self.n_bare_r3h2_to_make > r3h2_rate:
                        self.n_bare_r3h2_to_make -= r3h2_rate
                    else:
                        self.n_bare_r3h2.put(r3h2_rate - self.n_bare_r3h2_to_make)
                        self.n_bare_r3h2_made_today += r3h2_rate - self.n_bare_r3h2_to_make
                        self.n_bare_r3h2_to_make = 0

                    if self.n_bare_r3h3_to_make > r3h3_rate:
                        self.n_bare_r3h3_to_make -= r3h3_rate
                    else:
                        self.n_bare_r3h3.put(r3h3_rate - self.n_bare_r3h3_to_make)
                        self.n_bare_r3h3_made_today += r3h3_rate - self.n_bare_r3h3_to_make
                        self.n_bare_r3h3_to_make = 0

                    if self.n_bare_r4h0_to_make > r4h0_rate:
                        self.n_bare_r4h0_to_make -= r4h0_rate
                    else:
                        self.n_bare_r4h0.put(r4h0_rate - self.n_bare_r4h0_to_make)
                        self.n_bare_r4h0_made_today += r4h0_rate - self.n_bare_r4h0_to_make
                        self.n_bare_r4h0_to_make = 0

                    if self.n_bare_r4h1_to_make > r4h1_rate:
                        self.n_bare_r4h1_to_make -= r4h1_rate
                    else:
                        self.n_bare_r4h1.put(r4h1_rate - self.n_bare_r4h1_to_make)
                        self.n_bare_r4h1_made_today += r4h1_rate - self.n_bare_r4h1_to_make
                        self.n_bare_r4h1_to_make = 0

                    if self.n_bare_r5h0_to_make > r5h0_rate:
                        self.n_bare_r5h0_to_make -= r5h0_rate
                    else:
                        self.n_bare_r5h0.put(r5h0_rate - self.n_bare_r5h0_to_make)
                        self.n_bare_r5h0_made_today += r5h0_rate - self.n_bare_r5h0_to_make
                        self.n_bare_r5h0_to_make = 0

                    if self.n_bare_r5h1_to_make > r5h1_rate:
                        self.n_bare_r5h1_to_make -= r5h1_rate
                    else:
                        self.n_bare_r5h1.put(r5h1_rate - self.n_bare_r5h1_to_make)
                        self.n_bare_r5h1_made_today += r5h1_rate - self.n_bare_r5h1_to_make
                        self.n_bare_r5h1_to_make = 0


                    yield self.env.timeout(1)
                    
            except simpy.Interrupt:
                yield self.env.timeout(180)
        
        
    def MakePowerboards(self, r0_rate, r1_rate, r2_rate, r3_rate, r4_rate, r5_rate):
    
        while True:
            
            try:
            
                if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                    yield self.env.timeout(1)

                else:

                    if self.n_r0_powerboards_to_make > r0_rate:
                        self.n_r0_powerboards_to_make -= r0_rate
                    else:
                        self.n_r0_powerboards.put(r0_rate - self.n_r0_powerboards_to_make)
                        self.n_r0_powerboards_made_today += r0_rate - self.n_r0_powerboards_to_make
                        self.n_r0_powerboards_to_make = 0

                    if self.n_r1_powerboards_to_make > r1_rate:
                        self.n_r1_powerboards_to_make -= r1_rate
                    else:
                        self.n_r1_powerboards.put(r1_rate - self.n_r1_powerboards_to_make)
                        self.n_r1_powerboards_made_today += r1_rate - self.n_r1_powerboards_to_make
                        self.n_r1_powerboards_to_make = 0

                    if self.n_r2_powerboards_to_make > r2_rate:
                        self.n_r2_powerboards_to_make -= r2_rate
                    else:
                        self.n_r2_powerboards.put(r2_rate - self.n_r2_powerboards_to_make)
                        self.n_r2_powerboards_made_today += r2_rate - self.n_r2_powerboards_to_make
                        self.n_r2_powerboards_to_make = 0

                    if self.n_r3_powerboards_to_make > r3_rate:
                        self.n_r3_powerboards_to_make -= r3_rate
                    else:
                        self.n_r3_powerboards.put(r3_rate - self.n_r3_powerboards_to_make)
                        self.n_r3_powerboards_made_today += r3_rate - self.n_r3_powerboards_to_make
                        self.n_r3_powerboards_to_make = 0

                    if self.n_r4_powerboards_to_make > r4_rate:
                        self.n_r4_powerboards_to_make -= r4_rate
                    else:
                        self.n_r4_powerboards.put(r4_rate - self.n_r4_powerboards_to_make)
                        self.n_r4_powerboards_made_today += r4_rate - self.n_r4_powerboards_to_make
                        self.n_r4_powerboards_to_make = 0

                    if self.n_r5_powerboards_to_make > r5_rate:
                        self.n_r5_powerboards_to_make -= r5_rate
                    else:
                        self.n_r5_powerboards.put(r5_rate - self.n_r5_powerboards_to_make)
                        self.n_r5_powerboards_made_today += r5_rate - self.n_r5_powerboards_to_make
                        self.n_r5_powerboards_to_make = 0

                    yield self.env.timeout(1)
            
            except simpy.Interrupt:
                yield self.env.timeout(30)
            
    def ClearItemsMade(self):
        
        while True:
            
            self.n_bare_r0h0_made_today = 0
            self.n_bare_r0h1_made_today = 0
            self.n_bare_r1h0_made_today = 0
            self.n_bare_r1h1_made_today = 0
            self.n_bare_r2h0_made_today = 0
            self.n_bare_r3h0_made_today = 0
            self.n_bare_r3h1_made_today = 0
            self.n_bare_r3h2_made_today = 0
            self.n_bare_r3h3_made_today = 0
            self.n_bare_r4h0_made_today = 0
            self.n_bare_r4h1_made_today = 0
            self.n_bare_r5h0_made_today = 0
            self.n_bare_r5h1_made_today = 0

            self.n_r0_powerboards_made_today = 0
            self.n_r1_powerboards_made_today = 0
            self.n_r2_powerboards_made_today = 0
            self.n_r3_powerboards_made_today = 0
            self.n_r4_powerboards_made_today = 0
            self.n_r5_powerboards_made_today = 0
            
            yield self.env.timeout(1)
    
                        
                                        

    
    
def ReadEndcapPowerboardRates(env, list_of_powerboard_sites, input_csv):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        line = 0

        for row in csv_reader:
            
            if line == 0:
                line += 1
                continue

            for item in list_of_powerboard_sites:
                if item.name == row[0]:
                    process = env.process(item.MakePowerboards(float(row[1]), float(row[2]), float(row[3]), float(row[4]), float(row[5]), float(row[6])))
                    #env.process(model.ScheduleInterrupt(env, process, 300))
                    
                    
def ReadEndcapBareHybridRates(env,  list_of_bare_hybrid_sites, input_csv):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        line = 0

        for row in csv_reader:
            
            if line == 0:
                line += 1
                continue

            for item in list_of_bare_hybrid_sites:
                if item.name == row[0]:
                    process = env.process(item.MakeBareHybrids(float(row[1]), float(row[2]), float(row[3]), float(row[4]), float(row[5]), float(row[6]), float(row[7]),
                                                        float(row[8]), float(row[9]), float(row[10]), float(row[11]), float(row[12]), float(row[13])))
                    #env.process(model.ScheduleInterrupt(env, process, 300))
                    
def ReadEndcapModuleYields( input_csv, list_of_production_sites, list_of_module_yields):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        line = 0

        for row in csv_reader:
            
            if line == 0:
                line += 1
                continue
                
            for entry in row:
                list_of_module_yields.append(float(entry))

        for item in list_of_production_sites:
            item.r0_module_yield = list_of_module_yields[0] 
            item.r1_module_yield = list_of_module_yields[1]
            item.r2_module_yield = list_of_module_yields[2]
            item.r3_module_yield = list_of_module_yields[3]
            item.r4_module_yield = list_of_module_yields[4]
            item.r5_module_yield = list_of_module_yields[5]
            
def ReadEndcapHybridYields( input_csv, list_of_production_sites, list_of_hybrid_yields):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        line = 0

        for row in csv_reader:
            
            if line == 0:
                line += 1
                continue
                
            for entry in row:
                list_of_hybrid_yields.append(float(entry))

        for item in list_of_production_sites:
            item.r0h0_yield = list_of_hybrid_yields[0]
            item.r0h1_yield = list_of_hybrid_yields[1]
            item.r1h0_yield = list_of_hybrid_yields[2]
            item.r1h1_yield = list_of_hybrid_yields[3]
            item.r2h0_yield = list_of_hybrid_yields[4]
            item.r3h0_yield = list_of_hybrid_yields[5]
            item.r3h1_yield = list_of_hybrid_yields[6]
            item.r3h2_yield = list_of_hybrid_yields[7]
            item.r3h3_yield = list_of_hybrid_yields[8]
            item.r4h0_yield = list_of_hybrid_yields[9]
            item.r4h1_yield = list_of_hybrid_yields[10]
            item.r5h0_yield = list_of_hybrid_yields[11]
            item.r5h1_yield = list_of_hybrid_yields[12]
            
                                        
def ReadEndcapModuleRates( input_csv, list_of_production_sites, list_of_module_yields):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        line = 0
        
        for row in csv_reader:
            if line == 0:
                line += 1
                continue

            for item in list_of_production_sites:
                if item.name == row[0]:

                    total_rate = 0

                    target_rates = [0, 0, 0, 0, 0, 0]

                    for i in range(6):
                        row[i+1] = float(row[i+1])
                        target_rates[i] = row[i+1]*list_of_module_yields[i]

                    max_target_rate = max(target_rates)

                    if row[1] != 0:
                        item.make_r0_modules = True
                        item.r0_production_weight = target_rates[0]/max_target_rate
                        total_rate += row[1]

                    if row[2] != 0:
                        item.make_r1_modules = True
                        item.r1_production_weight = target_rates[1]/max_target_rate
                        total_rate += row[2]

                    if row[3] != 0:
                        item.make_r2_modules = True
                        item.r2_production_weight = target_rates[2]/max_target_rate
                        total_rate += row[3]

                    if row[4] != 0:
                        item.make_r3_modules = True
                        item.r3_production_weight = target_rates[3]/max_target_rate
                        total_rate += row[4]

                    if row[5] != 0:
                        item.make_r4_modules = True
                        item.r4_production_weight = target_rates[4]/max_target_rate
                        total_rate += row[5]

                    if row[6] != 0:
                        item.make_r5_modules = True
                        item.r5_production_weight = target_rates[5]/max_target_rate
                        total_rate += row[6]

                    item.module_production_capacity = total_rate


                    
                    
def ReadEndcapHybridRates(input_csv, list_of_production_sites, list_of_hybrid_yields, list_of_module_yields):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        line = 0
        
        for row in csv_reader:
            if line == 0:
                line += 1
                continue
                
            for item in list_of_production_sites:
                if item.name == row[0]:

                    total_rate = 0

                    target_rates = [0]*13
                    for i in range(13):
                        row[i+1] = float(row[i+1])
                        target_rates[i] = row[i+1]*list_of_hybrid_yields[i]
                        
                    target_rates[0] = target_rates[0]*list_of_module_yields[0]
                    target_rates[1] = target_rates[1]*list_of_module_yields[0]
                    target_rates[2] = target_rates[2]*list_of_module_yields[1]
                    target_rates[3] = target_rates[3]*list_of_module_yields[1]
                    target_rates[4] = target_rates[4]*list_of_module_yields[2]
                    target_rates[5] = target_rates[5]*list_of_module_yields[3]
                    target_rates[6] = target_rates[6]*list_of_module_yields[3]
                    target_rates[7] = target_rates[7]*list_of_module_yields[3]
                    target_rates[8] = target_rates[8]*list_of_module_yields[3]
                    target_rates[9] = target_rates[9]*list_of_module_yields[4]
                    target_rates[10] = target_rates[10]*list_of_module_yields[4]
                    target_rates[11] = target_rates[11]*list_of_module_yields[5]
                    target_rates[12] = target_rates[12]*list_of_module_yields[5]
                    max_target_rate = max(target_rates)

                    if row[1] != 0:
                        item.make_r0h0 = True
                        item.r0h0_production_weight = target_rates[0]/max_target_rate
                        total_rate += row[1] 

                    if row[2] != 0:
                        item.make_r0h1 = True
                        item.r0h1_production_weight = target_rates[1]/max_target_rate
                        total_rate += row[2]

                    if row[3] != 0:
                        item.make_r1h0 = True
                        item.r1h0_production_weight = target_rates[2]/max_target_rate
                        total_rate += row[3]

                    if row[4] != 0:
                        item.make_r1h1 = True
                        item.r1h1_production_weight = target_rates[3]/max_target_rate
                        total_rate += row[4]

                    if row[5] != 0:
                        item.make_r2h0 = True
                        item.r2h0_production_weight = target_rates[4]/max_target_rate
                        total_rate += row[5]

                    if row[6] != 0:
                        item.make_r3h0 = True
                        item.r3h0_production_weight = target_rates[5]/max_target_rate
                        total_rate += row[6]

                    if row[7] != 0:
                        item.make_r3h1 = True
                        item.r3h1_production_weight = target_rates[6]/max_target_rate
                        total_rate += row[7]

                    if row[8] != 0:
                        item.make_r3h2 = True
                        item.r3h2_production_weight = target_rates[7]/max_target_rate
                        total_rate += row[8]

                    if row[9] != 0:
                        item.make_r3h3 = True
                        item.r3h3_production_weight = target_rates[8]/max_target_rate
                        total_rate += row[9]

                    if row[10] != 0:
                        item.make_r4h0 = True
                        item.r4h0_production_weight = target_rates[9]/max_target_rate
                        total_rate += row[10]

                    if row[11] != 0:
                        item.make_r4h1 = True
                        item.r4h1_production_weight = target_rates[10]/max_target_rate
                        total_rate += row[11]

                    if row[12] != 0:
                        item.make_r5h0 = True
                        item.r5h0_production_weight = target_rates[11]/max_target_rate
                        total_rate += row[12]

                    if row[13] != 0:
                        item.make_r5h1 = True
                        item.r5h1_production_weight = target_rates[12]/max_target_rate
                        total_rate += row[13]

                    item.hybrid_production_capacity = total_rate



            
        