import os
import random
import csv
import math
import simpy
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
matplotlib.rc('xtick', labelsize=25)     
matplotlib.rc('ytick', labelsize=25)

OUTPUTDIR = 'output'
START_OFFSET = 60

SHIPMENTS_SENT = 0
SENSOR_SHIPMENTS = 0
ASIC_SHIPMENTS = 0 
BARE_HYBRID_SHIPMENTS = 0
COMPLETE_HYBRID_SHIPMENTS = 0
POWERBOARD_SHIPMENTS = 0
MODULE_SHIPMENTS = 0

class ShippingRoute(object):
    
    
    # Assuming periodic shipments between a specific sender and receiver
    def __init__(self, env, sender, receiver):
        self.env = env
        self.sender = sender
        self.receiver = receiver
        self.parts_held = []
    
    # Starts a shipment process
    def SendShipment(self, part_name, n_parts, transit_time, transit_sd, time_between_shipments, shipment_yield):
        global SHIPMENTS_SENT
        global SENSOR_SHIPMENTS
        global ASIC_SHIPMENTS
        global BARE_HYBRID_SHIPMENTS
        global COMPLETE_HYBRID_SHIPMENTS
        global POWERBOARD_SHIPMENTS
        global MODULE_SHIPMENTS
        while True:
            
            n_parts_to_ship = 0
            n_parts_shipped = 0
            
            # If sender doesn't have enough parts to send, then send as many as possible
            sender_stock = getattr(getattr(self.sender,part_name),'level')
            if n_parts > sender_stock:
                n_parts_to_ship = sender_stock
            else:
                n_parts_to_ship = n_parts
            if abs(n_parts_to_ship - round(n_parts_to_ship)) > 0.001:
                if random.uniform(0,1) < n_parts_to_ship - math.floor(n_parts_to_ship):
                    n_parts_shipped = math.ceil(n_parts_to_ship)
                else:
                    n_parts_shipped = math.floor(n_parts_to_ship)
            else:
                n_parts_shipped = n_parts_to_ship
            
            # If no parts to send, wait for next scheduled shipment to try again
            if n_parts_shipped == 0:
                yield self.env.timeout(time_between_shipments)
            # Send the parts
            else:
                getattr(self.sender, part_name).get(n_parts_shipped)
                self.env.process(self.ShipPart(self.receiver, part_name, n_parts_shipped, transit_time, transit_sd, shipment_yield))
                SHIPMENTS_SENT += 1
                if 'sensor' in part_name:
                    SENSOR_SHIPMENTS += 1
                elif 'abc' in part_name or 'hcc' in part_name:
                    ASIC_SHIPMENTS += 1
                elif 'bare' in part_name:
                    BARE_HYBRID_SHIPMENTS += 1
                elif 'complete' in part_name:
                    COMPLETE_HYBRID_SHIPMENTS += 1
                elif 'powerboard' in part_name:
                    POWERBOARD_SHIPMENTS += 1
                elif 'module' in part_name:
                    MODULE_SHIPMENTS += 1
                yield self.env.timeout(time_between_shipments)
    
    def SendPercentageShipment(self, partname, percent_to_ship, transit_time, transit_sd, time_between_shipments, shipment_yield):
        global SHIPMENTS_SENT
        global SENSOR_SHIPMENTS
        global ASIC_SHIPMENTS
        global BARE_HYBRID_SHIPMENTS
        global COMPLETE_HYBRID_SHIPMENTS
        global POWERBOARD_SHIPMENTS
        global MODULE_SHIPMENTS
        self.parts_held.append([partname, 0.0])
        
        while True:              
            
            sender_stock = getattr(getattr(self.sender,partname),'level')
            track_str = partname+'_made_today'
            n_items_made = getattr(self.sender,track_str)
            if n_items_made > 0:
                for entry in self.parts_held:
                    if entry[0] == partname:
                        getattr(self.sender, partname).get(n_items_made*percent_to_ship)
                        entry[1] += n_items_made*percent_to_ship
            elif n_items_made < 0:
                for entry in self.parts_held:
                    if entry[0] == partname:
                        getattr(self.sender, partname).put(-1*n_items_made*percent_to_ship)
                        entry[1] -= n_items_made*percent_to_ship
                
            if self.env.now % time_between_shipments == 0:
                
                for entry in self.parts_held:
                    if entry[0] == partname:
                        if abs(entry[1] - round(entry[1])) < 0.001:
                            n_parts_shipped = entry[1]

                        else:
                            if random.uniform(0,1) < entry[1] - math.floor(entry[1]):
                                n_parts_shipped = math.ceil(entry[1])
                                difference = math.ceil(entry[1]) - entry[1]
                                if sender_stock >= difference:
                                    getattr(self.sender, partname).get(difference)
                                else:
                                    if sender_stock > 0:
                                        difference -= sender_stock
                                        getattr(self.sender, partname).get(sender_stock)
                                    debt_str = partname+'_to_make'
                                    total_str = partname.replace('n_','total_')+'_made'
                                    new_total = getattr(self.sender, total_str) + difference
                                    setattr(self.sender, debt_str, difference)
                                    setattr(self.sender, total_str, new_total)
                                    
                            else:
                                n_parts_shipped = math.floor(entry[1])
                                getattr(self.sender, partname).put(entry[1] - math.floor(entry[1]))
                
                        if n_parts_shipped > 0:
                            self.env.process(self.ShipPart(self.receiver, partname, n_parts_shipped, transit_time, transit_sd, shipment_yield))
                            SHIPMENTS_SENT += 1
                            if 'sensor' in partname:
                                SENSOR_SHIPMENTS += 1
                            elif 'abc' in partname or 'hcc' in partname:
                                ASIC_SHIPMENTS += 1
                            elif 'bare' in partname:
                                BARE_HYBRID_SHIPMENTS += 1
                            elif 'complete' in partname:
                                COMPLETE_HYBRID_SHIPMENTS += 1
                            elif 'powerboard' in partname:
                                POWERBOARD_SHIPMENTS += 1
                            elif 'module' in partname:
                                MODULE_SHIPMENTS += 1
                        entry[1] = 0
                
                
            yield self.env.timeout(1)
            
            
            
    # The actual shipment process
    def ShipPart(self, receiver, part_name, n_items_shipped, average_transit_time, transit_sd, shipment_yield):
        # Randomise shipping time
        transit_time = int(round(random.gauss(average_transit_time,transit_sd)))
        if transit_time <= 0:
            transit_time = 1
        yield self.env.timeout(transit_time)
        n_items_arrived = n_items_shipped
       
        for i in range(0, int(round(n_items_shipped))):
            if random.uniform(0,1) > shipment_yield:
                n_items_arrived -= 1
        if n_items_arrived > 0:
            getattr(receiver, part_name).put(n_items_arrived)
        
        # Track items shipped
#         filename_shipped = str(part_name)+'_shipped_'+str(getattr(self.sender, 'name'))+'_to_'+str(getattr(receiver, 'name'))
#         output_file_shipped = os.path.join(os.getcwd(),OUTPUTDIR+'/'+filename_shipped)
# #         if os.path.exists(output_file_shipped):
# #             os.remove(output_file_shipped)
#         f = open(output_file_shipped, 'a+')
#         day = self.env.now
#         f.write(str(day)+', '+str(n_items_shipped)+'\n')
        


def ReadShipping(env, input_csv, ProductionSiteDictionary):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        line = 0
        
        for row in csv_reader:
            if line == 0:
                line += 1
                continue
            
            sender = row[0]
            receiver = row[1]
            part_name = row[2]
            percentage = False
            
            if 'true' in row[3].lower():
                percentage = True
            size = float(row[4])
            time_between_shipments = float(row[5])
            transit_time = float(row[6])
            transit_time_standard_deviation = float(row[7])
            shipping_yield = float(row[8])
            
            route = ShippingRoute(env, ProductionSiteDictionary[sender], ProductionSiteDictionary[receiver])
            if percentage == True:
                shipment = route.SendPercentageShipment(part_name, size, transit_time, transit_time_standard_deviation, time_between_shipments, shipping_yield)
                env.process(shipment)
            else:
                shipment = route.SendShipment(part_name, size, transit_time, transit_time_standard_deviation, time_between_shipments, shipping_yield)
                env.process(shipment)
                

class DataCollector(object):
    
    # Input list of production sites whose inventory needs to be tracked
    def __init__(self, env, *args):
        self.env = env
        self.list_of_production_sites = []
        for arg in args:
            self.list_of_production_sites.append(arg)
            
            
    def GetInventory(self):
        
        for production_site in self.list_of_production_sites:
                for item in dir(production_site):
                    if str(item).startswith('n_') == True:
                        filename = str(production_site.name)+'_'+str(item)
                        output_file = os.path.join(os.getcwd(),OUTPUTDIR+'/txt/'+filename)
                        if os.path.exists(output_file):
                            os.remove(output_file)
                    if str(item).startswith('total_'):
                        filename = str(production_site.name)+'_'+str(item)
                        output_file = os.path.join(os.getcwd(),OUTPUTDIR+'/txt/'+filename)
                        if os.path.exists(output_file):
                            os.remove(output_file)
                            
        
        while True:
            if not os.path.exists(OUTPUTDIR):
                os.makedirs(OUTPUTDIR)
            if not os.path.exists(OUTPUTDIR+'/txt/'):
                os.makedirs(OUTPUTDIR+'/txt/')
            for production_site in self.list_of_production_sites:
                for item in dir(production_site):
                    if (str(item).startswith('n_') == True) and (str(item).endswith('made') == False) and (str(item).endswith('made_today') == False) and (str(item).endswith('to_make') == False):
                        filename = str(production_site.name)+'_'+str(item)
                        output_file = os.path.join(os.getcwd(),OUTPUTDIR+'/txt/'+filename)
                        f = open(output_file, 'a+')
                        day = self.env.now
                        n_items = getattr(getattr(production_site, item),'level')
                        f.write(str(day)+', '+str(n_items)+'\n')
                    if (str(item).startswith('n_') == True) and ((str(item).endswith('made') == True) or (str(item).endswith('made_today') == True) or (str(item).endswith('to_make') == True)):
                        filename = str(production_site.name)+'_'+str(item)
                        output_file = os.path.join(os.getcwd(),OUTPUTDIR+'/txt/'+filename)
                        f = open(output_file, 'a+')
                        day = self.env.now
                        n_items = getattr(production_site, item)
                        f.write(str(day)+', '+str(n_items)+'\n')
                    if str(item).startswith('total_'):
                        filename = str(production_site.name)+'_'+str(item)
                        output_file = os.path.join(os.getcwd(),OUTPUTDIR+'/txt/'+filename)
                        f = open(output_file, 'a+')
                        day = self.env.now
                        n_items = getattr(production_site, item)
                        f.write(str(day)+', '+str(n_items)+'\n')
            yield self.env.timeout(1)
            


def StaticMakePlots( list_of_production_sites):
    
    for production_site in list_of_production_sites:
        for item in dir(production_site):
            if ((str(item).startswith('n_') == True) or (str(item).startswith('total_') == True) ):
                
                filename = str(production_site.name)+'_'+str(item)
                output_dir = os.path.join(os.getcwd(),OUTPUTDIR)
                output_file = output_dir+'/txt/'+filename
                plot_data = open(output_file, 'r').read()
                lines = plot_data.split('\n')
                x_values = []
                y_values = []
                for line in lines:
                    if len(line) > 1:
                        x, y = line.split(', ')
                        x_values.append(float(x))
                        y_values.append(float(y))
                        
                if all(y == 0 for y in y_values) == False:
                    plot = plt.figure(figsize=(14,14))
                    ax1 = plot.add_subplot(1,1,1)
                    l1 = ax1.plot(x_values, y_values)
                    if 'petal' in filename or 'stave' in filename:
                        ax1.axvline(x = START_OFFSET, ymin=0, ymax=1)
                        ax1.axvline(x = START_OFFSET + 154, ymin=0, ymax=1)
                        ax1.axvline(x = START_OFFSET + 462, ymin=0, ymax=1)
                        ax1.axvline(x = START_OFFSET + 770, ymin=0, ymax=1)
                        ax1.axvline(x = START_OFFSET + 924, ymin=0, ymax=1)
                    if 'petal' in filename:
                        ax1.set_ylim(0,120)
                        if 'Freiburg' in filename:
                            ax1.axhline(y = 108, color='black')
                        if 'Vancouver' in filename:
                            ax1.axhline(y = 84, color='black')
                        if 'Valencia' in filename or 'Hamburg' in filename:
                            ax1.axhline(y = 96, color='black')
#                     ax1.axvline(x = 300, ymin=0, ymax=1, color='red')
#                     ax1.axvline(x = 480, ymin=0, ymax=1, color='red')
                    if 'stave' in filename:
                        if 'ls' in filename:
                            ax1.axhline(y = 128, color='black')
                        if 'ss' in filename:
                            ax1.axhline(y = 68, color='black')
                    ax1.set_xlabel("Days Elapsed", fontsize = 25)
                    ax1.set_ylabel("Item Count", fontsize = 25)
                    
                    plots_dir = output_dir+'/plots/'
                    if not os.path.exists(plots_dir):
                        os.makedirs(plots_dir)
                    plt.savefig(plots_dir+filename, bbox_inches='tight')
                    plt.close()

                    
def ScheduleSiteShutdown(env, site, start_day, end_day):
    yield env.timeout(start_day)
    site.rest_until_date = end_day
                    
def PrintProgress(env, days_between_print):
    while True:
        if env.now % days_between_print == 0:
            print('Currently on day :'+str(env.now))
        yield env.timeout(1)
        
