import model
import os
import random
import math
import simpy
import csv
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import style
matplotlib.rc('xtick', labelsize=25)     
matplotlib.rc('ytick', labelsize=25)

OUTPUTDIR = 'barrel_output'
START_OFFSET = 30
    
class ProductionSite(object):
    

    def __init__(self, env, name):
        self.name = name
        self.env = env
        # Inventory
        self.n_abcs = simpy.Container(env)
        self.n_hccs = simpy.Container(env)
        self.n_bare_hx = simpy.Container(env)
        self.n_bare_hy = simpy.Container(env)
        self.n_complete_hx = simpy.Container(env)
        self.n_complete_hy = simpy.Container(env)
        self.n_pre_burn_in_hx = simpy.Container(env)
        self.n_pre_burn_in_hy = simpy.Container(env)
        self.n_ss_sensors = simpy.Container(env)
        self.n_ls_sensors = simpy.Container(env)
        self.n_powerboards = simpy.Container(env)
        self.n_ss_modules = simpy.Container(env)
        self.n_ls_modules = simpy.Container(env)
        self.n_ss_staves = simpy.Container(env)
        self.n_ls_staves = simpy.Container(env)
        # Production Settings
        self.rest_until_date = 0
        self.available_fte = 100
        self.ls_rate = 0.0
        self.ss_rate = 0.0
        self.ls_target = 0
        self.ss_target = 0
        self.hybrid_production_capacity = 0
        self.hybrid_production_sd = 0.0
        self.hybrid_burn_in_yield = 0.99
        self.module_production_capacity = 0.0
        self.module_production_sd = 0.0
        self.make_hx = True
        self.make_hy = False
        self.hx_production_weight = 1.0
        self.hy_production_weight = 1.0
        self.hx_yield = 1.0
        self.hy_yield = 1.0
        self.make_ss_modules = False
        self.make_ls_modules = True
        self.ss_production_weight= 1.0
        self.ls_production_weight= 1.0
        self.ss_module_yield = 1.0
        self.ls_module_yield = 1.0
        # Tracking Total Items Produced
        self.total_complete_hx_made = 0
        self.total_complete_hy_made = 0
        self.total_ss_modules_made = 0
        self.total_ls_modules_made = 0
        
        # 
        
        self.n_ss_modules_made = 0
        self.n_ls_modules_made = 0
        self.n_hxs_made = 0
        self.n_hys_made = 0
        
        self.n_ss_modules_made_today = 0
        self.n_ls_modules_made_today = 0
        self.n_complete_hx_made_today = 0
        self.n_complete_hy_made_today = 0
        self.hxs_in_burn_in = 0
        self.hys_in_burn_in = 0
        self.burn_in_complete = False
        self.prior_hxs_made = 0
        
        self.n_ss_modules_to_make = 0
        self.n_ls_modules_to_make = 0
        self.n_modules_to_make = 0
        self.n_complete_hx_to_make = 0
        self.n_complete_hy_to_make = 0
        self.n_hybrids_to_make = 0
        
    def MakeParts(self):
        
        while True: 
        
            # Don't work if it's the weekend
            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)

            else:
                self.n_ss_modules_made = 0
                self.n_ls_modules_made = 0
                self.n_hxs_made = 0
                self.n_hys_made = 0


                if self.total_ls_modules_made < self.ls_target:
                    self.module_production_capacity = self.ls_rate

                elif self.total_ls_modules_made >= self.ls_target and self.prior_hxs_made == 0:
                    self.prior_hxs_made = self.total_complete_hx_made - self.n_complete_hx.level
                    self.module_production_capacity = self.ss_rate
                    self.make_hx = True
                    self.make_hy = True
                    self.make_ss_modules = True
                    self.make_ls_modules = False

                if self.total_ss_modules_made > self.ss_target:
                    self.make_ss_modules = False


                #####################
                # Module production #
                #####################


                n_types_of_hybrids = self.GetNumberOfHybridTypes()
                n_types_of_modules = 1

                if self.module_production_capacity > 0.01:

                    ss_module_capacity_reached = False
                    if self.make_ss_modules == False:
                        ss_module_capacity_reached = True

                    ls_module_capacity_reached = False
                    if self.make_ls_modules == False:
                        ls_module_capacity_reached = True


                    if self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0 > self.available_fte:
                        corrected_production_capacity = ( (self.module_production_capacity*self.available_fte) 
                                                          / (self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0) )
                    else:
                        corrected_production_capacity = self.module_production_capacity
                    actual_production_capacity = random.gauss(corrected_production_capacity, self.module_production_sd)
                    leftover_production_capacity = actual_production_capacity

                    self.n_modules_to_make += (self.n_ls_modules_to_make + self.n_ss_modules_to_make)

                    self.n_ls_modules_to_make = 0
                    self.n_ss_modules_to_make = 0

                    if self.n_modules_to_make > 0:
                        if self.n_modules_to_make > leftover_production_capacity:
                            self.n_modules_to_make -= leftover_production_capacity
                            leftover_production_capacity = 0
                        else:
                            leftover_production_capacity -= self.n_modules_to_make
                            self.n_modules_to_make = 0

                    while (leftover_production_capacity > 0.01 and (ss_module_capacity_reached == False or ls_module_capacity_reached == False)):

                        leftover_production_capacity = (actual_production_capacity -
                                                        self.n_ss_modules_made - self.n_ls_modules_made)

                        total_n_modules_made = 0

                        if self.make_ss_modules == True:
                            total_n_modules_made += (self.total_ss_modules_made + self.n_ss_modules_made)/self.ss_production_weight
                        if self.make_ls_modules == True:
                            total_n_modules_made += (self.total_ls_modules_made + self.n_ls_modules_made)/self.ls_production_weight


                        average_n_modules_made = total_n_modules_made/n_types_of_modules

                        # Make more of any modules that are below average in total number produced
                        while ss_module_capacity_reached == False or ls_module_capacity_reached == False:

                            # Try to make one more ss Module
                            if ss_module_capacity_reached == False:
                                if (self.n_ss_modules_made + 1 > self.n_ss_sensors.level or
                                    self.n_ss_modules_made + 1 > self.n_powerboards.level or
                                    self.n_ss_modules_made + 1 > self.n_complete_hx.level or
                                    self.n_ss_modules_made + 1 > self.n_complete_hy.level):
                                    ss_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        self.n_ss_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        self.n_ss_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more ls Module
                            if ls_module_capacity_reached == False:
                                if (self.n_ls_modules_made + 1 > self.n_ls_sensors.level or
                                    self.n_ls_modules_made + 1 > self.n_powerboards.level or
                                    self.n_ls_modules_made + 1 > self.n_complete_hx.level):
                                    ls_module_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        self.n_ls_modules_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        self.n_ls_modules_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break


                    # Make the modules
                    if self.n_ss_modules_made > 0:
                        self.MakeSSModules()
                    if self.n_ls_modules_made > 0:
                        self.MakeLSModules()


                #####################
                # Hybrid production #
                #####################

                if self.hybrid_production_capacity > 0.01:

                    hx_capacity_reached = False
                    if self.make_hx == False:
                        hx_capacity_reached = True

                    hy_capacity_reached = False                
                    if self.make_hy == False:
                        hy_capacity_reached = True


                    if self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0 > self.available_fte:
                        corrected_production_capacity = (self.hybrid_production_capacity*self.available_fte) / (self.hybrid_production_capacity*0.3 + self.module_production_capacity*1.0)
                    else:
                        corrected_production_capacity = self.hybrid_production_capacity
                    actual_production_capacity = random.gauss(corrected_production_capacity, self.hybrid_production_sd)
                    leftover_production_capacity = actual_production_capacity

                    self.n_hybrids_to_make += (self.n_complete_hx_to_make + self.n_complete_hy_to_make)

                    self.n_complete_hx_to_make = 0
                    self.n_complete_hy_to_make = 0

                    if self.n_hybrids_to_make > 0:
                        if self.n_hybrids_to_make > leftover_production_capacity:
                            self.n_hybrids_to_make -= leftover_production_capacity
                            leftover_production_capacity = 0
                        else:
                            leftover_production_capacity -= self.n_hybrids_to_make
                            self.n_hybrids_to_make = 0

                    while ( leftover_production_capacity > 0.01 and (hx_capacity_reached == False or hy_capacity_reached == False)):


                        leftover_production_capacity = (actual_production_capacity - self.n_hxs_made - self.n_hys_made )

                        total_n_hybrids_made = 0

                        if self.make_hx == True:
                            total_n_hybrids_made += (self.total_complete_hx_made + self.n_hxs_made)/self.hx_production_weight
                        if self.make_hy == True:
                            total_n_hybrids_made += (self.total_complete_hy_made + self.n_hys_made + self.prior_hxs_made)/self.hy_production_weight


                        average_n_hybrids_made = total_n_hybrids_made/n_types_of_hybrids

                        # Make more of any hybrids that are below average in total number produced
                        while ( leftover_production_capacity > 0.01 and (((self.total_complete_hx_made + self.n_hxs_made)/self.hx_production_weight < average_n_hybrids_made 
                                                                          and hx_capacity_reached == False)
                                                                        or ((self.total_complete_hy_made + self.n_hys_made + self.prior_hxs_made)/self.hy_production_weight < average_n_hybrids_made 
                                                                            and hy_capacity_reached == False))):

                            total_n_hybrids_made = 0

                            if self.make_hx == True:
                                total_n_hybrids_made += (self.total_complete_hx_made + self.n_hxs_made)/self.hx_production_weight
                            if self.make_hy == True:
                                total_n_hybrids_made += (self.total_complete_hy_made + self.n_hys_made + self.prior_hxs_made)/self.hy_production_weight


                            average_n_hybrids_made = total_n_hybrids_made/n_types_of_hybrids

                            # Try to make one more hx
                            if hx_capacity_reached == False and (self.total_complete_hx_made + self.n_hxs_made)/self.hx_production_weight < average_n_hybrids_made:
                                if ( self.n_hxs_made + 1 > self.n_bare_hx.level or
                                     self.n_hxs_made + 1 > self.n_hccs.level or
                                     self.n_hxs_made + 1 > int(self.n_abcs.level/10) ):
                                    hx_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        self.n_hxs_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        self.n_hxs_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if abs(leftover_production_capacity) <= 0.01:
                                        break

                            # Try to make one more hy
                            if hy_capacity_reached == False and (self.total_complete_hy_made + self.n_hys_made + self.prior_hxs_made)/self.hy_production_weight < average_n_hybrids_made:
                                if ( self.n_hys_made + 1 > self.n_bare_hy.level or
                                     self.n_hys_made + 1 > self.n_hccs.level or
                                     self.n_hys_made + 1 > int(self.n_abcs.level/10) ):
                                    hy_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        self.n_hys_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        self.n_hys_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break




                        # If equal number of modules have been produced so far, then randomly select which ones to make one more of
                        random_list = list(range(0,2))
                        random.shuffle(random_list)

                        for itr in random_list:

                            # Try to make one more hx
                            if hx_capacity_reached == False and itr == 0:
                                if ( self.n_hxs_made + 1 > self.n_bare_hx.level or
                                     self.n_hxs_made + 1 > self.n_hccs.level or
                                     self.n_hxs_made + 1 > int(self.n_abcs.level/10) ):
                                    hx_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        self.n_hxs_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        self.n_hxs_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break

                            # Try to make one more hy
                            if hy_capacity_reached == False and itr == 1:
                                if ( self.n_hys_made + 1 > self.n_bare_hy.level or
                                     self.n_hys_made + 1 > self.n_hccs.level or
                                     self.n_hys_made + 1 > int(self.n_abcs.level/10) ):
                                    hy_capacity_reached = True
                                else:
                                    if leftover_production_capacity > 1:
                                        self.n_hys_made += 1
                                        leftover_production_capacity -= 1
                                    else:
                                        self.n_hys_made += leftover_production_capacity
                                        leftover_production_capacity = 0
                                    if leftover_production_capacity < 0.01:
                                        break


                    # Make the hybrids
                    if self.n_hxs_made > 0:
                        self.MakeHX()
                    if self.n_hys_made > 0:
                        self.MakeHY()


            yield self.env.timeout(1)
    
    
    def HybridBurnIn(self, burn_in_capacity):
        
        while True:
            
            if self.burn_in_complete == True:
                for n in range(self.hxs_in_burn_in):
                    if random.uniform(0,1) < self.hybrid_burn_in_yield:
                        self.n_complete_hx.put(1)
                        self.total_complete_hx_made += 1
                        self.n_complete_hx_made_today += 1
                for n in range(self.hys_in_burn_in):
                    if random.uniform(0,1) < self.hybrid_burn_in_yield:
                        self.n_complete_hy.put(1)    
                        self.total_complete_hy_made += 1
                        self.n_complete_hy_made_today += 1
                self.hxs_in_burn_in = 0
                self.hys_in_burn_in = 0
                self.burn_in_complete = False
            
            n_hybrids_to_burn_in = (self.n_pre_burn_in_hx.level + self.n_pre_burn_in_hy.level)
            n_hxs_to_burn_in = self.n_pre_burn_in_hx.level
            n_hys_to_burn_in = self.n_pre_burn_in_hy.level
            
            
            if n_hybrids_to_burn_in == 0:
                yield self.env.timeout(7)
                
                
            elif n_hybrids_to_burn_in > burn_in_capacity:
                
                while n_hybrids_to_burn_in > burn_in_capacity:
                    
                    if n_hxs_to_burn_in > 0:
                        n_hxs_to_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                    if n_hys_to_burn_in > 0:
                        n_hys_to_burn_in -= 1
                        n_hybrids_to_burn_in -= 1
                        if n_hybrids_to_burn_in <= burn_in_capacity:
                            break
                                    
                if n_hxs_to_burn_in > 0:
                    self.n_pre_burn_in_hx.get(n_hxs_to_burn_in)
                    self.hxs_in_burn_in = n_hxs_to_burn_in
                if n_hys_to_burn_in > 0:
                    self.n_pre_burn_in_hy.get(n_hys_to_burn_in)
                    self.hys_in_burn_in = n_hys_to_burn_in
                    
                yield self.env.timeout(7)

                self.burn_in_complete = True
                    
            else:
                
                if n_hxs_to_burn_in > 0:
                    self.n_pre_burn_in_hx.get(n_hxs_to_burn_in)
                    self.hxs_in_burn_in = n_hxs_to_burn_in
                if n_hys_to_burn_in > 0:
                    self.n_pre_burn_in_hy.get(n_hys_to_burn_in)
                    self.hys_in_burn_in = n_hys_to_burn_in
                    

                yield self.env.timeout(7)

                self.burn_in_complete = True

            
        
    def MakeHX(self):
        
        n_items_made = 0

        if self.n_hxs_made - math.floor(self.n_hxs_made) > 0.01:
            if random.uniform(0,1) <= self.n_hxs_made - math.floor(self.n_hxs_made):
                n_items_made = int(round(math.ceil(self.n_hxs_made)))
            elif random.uniform(0,1) > self.n_hxs_made - math.floor(self.n_hxs_made):
                n_items_made = int(round(math.floor(self.n_hxs_made)))
        else:
            n_items_made = int(round(self.n_hxs_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(10*n_items_made)
            self.n_hccs.get(n_items_made)
            self.n_bare_hx.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > self.hx_yield:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_hx.put(n_items_made)
                


            
    def MakeHY(self):       
        
        n_items_made = 0
        
        if self.n_hys_made - math.floor(self.n_hys_made) > 0.01:
            if random.uniform(0,1) <= self.n_hys_made - math.floor(self.n_hys_made):
                n_items_made = int(round(math.ceil(self.n_hys_made)))
            elif random.uniform(0,1) > self.n_hys_made - math.floor(self.n_hys_made):
                n_items_made = int(round(math.floor(self.n_hys_made)))
        else:
            n_items_made = int(round(self.n_hys_made))
        if n_items_made > 0:
            # Make items
            self.n_abcs.get(10*n_items_made)
            self.n_hccs.get(n_items_made)
            self.n_bare_hy.get(n_items_made)
            for i in range(0,int(round(n_items_made))):
                if random.uniform(0,1) > self.hy_yield:
                    n_items_made -= 1
            if n_items_made > 0:
                self.n_pre_burn_in_hy.put(n_items_made)
                
                

                
    def MakeSSModules(self):      
        

        if math.floor(self.n_ss_modules.level + self.n_ss_modules_made) - math.floor(self.n_ss_modules.level) >= 1:   
            # Make items
            for i in range(math.floor(self.n_ss_modules.level), math.floor(self.n_ss_modules.level + self.n_ss_modules_made)):
                self.n_ss_sensors.get(1)
                self.n_complete_hx.get(1)
                self.n_complete_hy.get(1)
                self.n_powerboards.get(1)
                if random.uniform(0,1) > self.ss_module_yield:
                    self.n_ss_modules_made -= 1
                    
        if self.n_ss_modules_made < 0:
            self.n_ss_modules.get(abs(self.n_ss_modules_made))
            self.n_ss_modules_made_today += self.n_ss_modules_made
            self.total_ss_modules_made += self.n_ss_modules_made
        elif self.n_ss_modules_made > 0:
            self.n_ss_modules.put(self.n_ss_modules_made)
            self.n_ss_modules_made_today += self.n_ss_modules_made
            self.total_ss_modules_made += self.n_ss_modules_made
            
    def MakeLSModules(self):
        
        if math.floor(self.n_ls_modules.level + self.n_ls_modules_made) - math.floor(self.n_ls_modules.level) >= 1:   
            # Make items
            for i in range(math.floor(self.n_ls_modules.level), math.floor(self.n_ls_modules.level + self.n_ls_modules_made)):
                self.n_ls_sensors.get(1)
                self.n_complete_hx.get(1)
                self.n_powerboards.get(1)
                if random.uniform(0,1) > self.ls_module_yield:
                    self.n_ls_modules_made -= 1
                    
        if self.n_ls_modules_made < 0:
            self.n_ls_modules.get(abs(self.n_ls_modules_made))
            self.n_ls_modules_made_today += self.n_ls_modules_made
            self.total_ls_modules_made += self.n_ls_modules_made
        elif self.n_ls_modules_made > 0:
            self.n_ls_modules.put(self.n_ls_modules_made)
            self.n_ls_modules_made_today += self.n_ls_modules_made
            self.total_ls_modules_made += self.n_ls_modules_made
            

                    
    def MakeStaves(self, days_to_make_stave, yield_fraction):
        
        while True:
            # Don't work if it's the weekend
            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)
                
            else: 
                if (self.n_ls_modules.level >= 28):
                    self.n_ls_modules.get(28)
                    if random.uniform(0,1) > yield_fraction:
                        yield self.env.timeout(1)
                    else:
                        self.n_ls_staves.put(1)
                        yield self.env.timeout(days_to_make_stave)
                elif (self.n_ss_modules.level >= 28):
                    self.n_ss_modules.get(28)
                    if random.uniform(0,1) > yield_fraction:
                        yield self.env.timeout(1)
                    else:
                        self.n_ss_staves.put(1)
                        yield self.env.timeout(days_to_make_stave)
                else:
                    yield self.env.timeout(1)
                        
    def GetNumberOfHybridTypes(self):
        
        n_types_of_hybrids = 0
        
        if self.make_hx == True:
            n_types_of_hybrids += 1
        if self.make_hy == True:
            n_types_of_hybrids += 1
            
        return n_types_of_hybrids
        
    def ClearItemsMade(self):
        while True:
            self.n_complete_hx_made_today = 0
            self.n_complete_hy_made_today = 0
            self.n_ls_modules_made_today = 0
            self.n_ss_modules_made_today = 0
            yield self.env.timeout(1)


class SensorTestSite(object):
    
    def __init__(self, env, name):
        self.env = env
        self.name = name
        
        self.rest_until_date = 0

        
        self.test_rate = 0.0
        self.test_yield = 1.0
            
        self.n_untested_ls_sensors = simpy.Container(env)
        self.n_untested_ss_sensors = simpy.Container(env)
        
        self.n_ls_sensors = simpy.Container(env)
        self.n_ss_sensors = simpy.Container(env)
        
        self.n_abcs = simpy.Container(env)
        self.n_hccs = simpy.Container(env)
        
        # tracking
        
        self.total_ls_sensors_made = 0
        self.total_ss_sensors_made = 0
        
        # shipping
        
        self.n_ls_sensors_made_today = 0
        self.n_ss_sensors_made_today = 0
        
        self.n_abcs_made_today = 0
        self.n_hccs_made_today = 0
        
        self.n_ls_sensors_to_make = 0
        self.n_ss_sensors_to_make = 0
        self.n_sensors_to_make = 0
        
        self.n_abcs_to_make = 0
        self.n_hccs_to_make = 0
        
    def ScheduleSensorReception(self, arrival_date, n_ls_received, n_ss_received):
               
        yield self.env.timeout(arrival_date)
        if n_ls_received > 0:
            self.n_untested_ls_sensors.put(n_ls_received)
        if n_ss_received > 0:
            self.n_untested_ss_sensors.put(n_ss_received)    
            
                    
    def ReadSensorDeliverySchedule(self, input_csv):
            
        with open(input_csv) as csv_file:
            csv_reader = csv.reader(csv_file, delimiter=',')

            line = 0

            for row in csv_reader:

                if line == 0:
                    line += 1
                    continue
                        
                arrival_date = int(row[0])
                n_ls_received = float(row[1])
                n_ss_received = float(row[2])

                self.env.process(self.ScheduleSensorReception(arrival_date, n_ls_received, n_ss_received))
        
    def TestSensors(self):
            
        while True:
                
            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)
                
            else:
                
                if random.uniform(0,1) > self.test_rate - math.floor(self.test_rate):
                    n_sensors_to_test = math.floor(self.test_rate)
                else:
                    n_sensors_to_test = math.ceil(self.test_rate)

                    
                self.n_sensors_to_make += (self.n_ls_sensors_to_make + self.n_ss_sensors_to_make)
                    
                self.n_ls_sensors_to_make = 0
                self.n_ss_sensors_to_make = 0

                if self.n_sensors_to_make > n_sensors_to_test:
                    self.n_sensors_to_make -= n_sensors_to_test
                    n_sensors_to_test = 0
                else:
                    n_sensors_to_test -= self.n_sensors_to_make
                    self.n_sensors_to_make = 0


                while n_sensors_to_test > 0 and (self.n_untested_ls_sensors.level > 0 or self.n_untested_ss_sensors.level > 0):

                        if self.n_untested_ls_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_ls_sensors.get(1)
                            self.total_ls_sensors_made += 1
                            if random.uniform(0,1) < self.test_yield:
                                self.n_ls_sensors.put(1)
                                self.n_ls_sensors_made_today += 1
                                self.total_ls_sensors_made += 1
                            if n_sensors_to_test == 0:
                                break
                        elif self.n_untested_ss_sensors.level > 0:
                            n_sensors_to_test -= 1
                            self.n_untested_ss_sensors.get(1)
                            if random.uniform(0,1) < self.test_yield:
                                self.n_ss_sensors.put(1)
                                self.n_ss_sensors_made_today += 1
                                self.total_ss_sensors_made += 1
                            if n_sensors_to_test == 0:
                                break


                yield self.env.timeout(1)
                
        

                
    def TestABCs(self, daily_rate, abc_test_yield):

        while True:

            if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                yield self.env.timeout(1)

            for i in range(daily_rate - self.n_abcs_to_make):
                if random.uniform(0,1) < abc_test_yield:
                    self.n_abcs.put(1)
                    self.n_abcs_made_today += 1
                    
            self.n_abcs_to_make = 0
                        
            yield self.env.timeout(1)

    def ClearItemsMade(self):
        while True:
            self.n_ls_sensors_made_today = 0
            self.n_ss_sensors_made_today = 0

            self.n_abs_made_today = 0
            self.n_hccs_made_today = 0

            yield self.env.timeout(1)
        
            
class RawPartMaker(object):
    
    def __init__(self, env, name):
        self.env = env
        self.name = name
        
        self.rest_until_date = 0
        
        self.n_bare_hx = simpy.Container(env)
        self.n_bare_hy = simpy.Container(env)
        self.n_powerboards = simpy.Container(env)
        
        self.ls_target = 0
        self.bare_hybrid_ls_rate = 0
        self.bare_hybrid_ss_rate = 0
        self.powerboard_ls_rate = 0
        self.powerboard_ss_rate = 0
        
        # tracking stuff
        
        self.total_bare_hx_made = 0
        self.total_bare_hy_made = 0
        
        self.total_powerboards_made = 0
        
        self.n_bare_hx_made_today = 0
        self.n_bare_hy_made_today = 0
        
        self.n_powerboards_made_today = 0
        
        self.n_bare_hx_to_make = 0
        self.n_bare_hy_to_make = 0
        
        self.n_powerboards_to_make = 0
        
        

    def MakeBareHybrids(self):
        
        while True:
            
            try:
            
                if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                    yield self.env.timeout(1)

                else:
                    
                    if self.total_bare_hx_made < self.ls_target:
                        
                        if self.n_bare_hx_to_make > self.bare_hybrid_ls_rate:
                            self.n_bare_hx_to_make -= self.bare_hybrid_ls_rate
                        else:
                            self.n_bare_hx.put(self.bare_hybrid_ls_rate - self.n_bare_hx_to_make)
                            self.n_bare_hx_made_today += self.bare_hybrid_ls_rate - self.n_bare_hx_to_make
                            self.total_bare_hx_made += self.bare_hybrid_ls_rate - self.n_bare_hx_to_make
                            self.n_bare_hx_to_make = 0
                            
                    elif self.total_bare_hx_made >= self.ls_target:
                        
                        if self.n_bare_hx_to_make > self.bare_hybrid_ss_rate/2:
                            self.n_bare_hx_to_make -= self.bare_hybrid_ss_rate/2
                        else:
                            self.n_bare_hx.put(self.bare_hybrid_ss_rate/2 - self.n_bare_hx_to_make)
                            self.n_bare_hx_made_today += self.bare_hybrid_ss_rate/2 - self.n_bare_hx_to_make
                            self.total_bare_hx_made += self.bare_hybrid_ss_rate - self.n_bare_hx_to_make
                            self.n_bare_hx_to_make = 0

                        if self.n_bare_hy_to_make > self.bare_hybrid_ss_rate/2:
                            self.n_bare_hy_to_make -= self.bare_hybrid_ss_rate/2
                        else:
                            self.n_bare_hy.put(self.bare_hybrid_ss_rate/2 - self.n_bare_hy_to_make)
                            self.n_bare_hy_made_today += self.bare_hybrid_ss_rate/2 - self.n_bare_hy_to_make
                            self.total_bare_hy_made += self.bare_hybrid_ss_rate - self.n_bare_hy_to_make
                            self.n_bare_hy_to_make = 0


                    yield self.env.timeout(1)
                    
            except simpy.Interrupt:
                yield self.env.timeout(180)
        
        
    def MakePowerboards(self):
    
        while True:
            
            try:
            
                if self.env.now % 7 >= 5 or self.env.now < self.rest_until_date:
                    yield self.env.timeout(1)

                else:
                    
                    if self.total_bare_hx_made < self.ls_target:

                        if self.n_powerboards_to_make > self.powerboard_ls_rate:
                            self.n_powerboards_to_make -= self.powerboard_ls_rate
                        else:
                            self.n_powerboards.put(self.powerboard_ls_rate - self.n_powerboards_to_make)
                            self.n_powerboards_made_today += self.powerboard_ls_rate - self.n_powerboards_to_make
                            self.total_powerboards_made += self.powerboard_ls_rate - self.n_powerboards_to_make
                            self.n_powerboards_to_make = 0
                            
                    elif self.total_bare_hx_made >= self.ls_target:
                        
                        if self.n_powerboards_to_make > self.powerboard_ss_rate:
                            self.n_powerboards_to_make -= self.powerboard_ss_rate
                        else:
                            self.n_powerboards.put(self.powerboard_ss_rate - self.n_powerboards_to_make)
                            self.n_powerboards_made_today += self.powerboard_ss_rate - self.n_powerboards_to_make
                            self.total_powerboards_made += self.powerboard_ss_rate - self.n_powerboards_to_make
                            self.n_powerboards_to_make = 0

                    yield self.env.timeout(1)
            
            except simpy.Interrupt:
                yield self.env.timeout(30)
            
    def ClearItemsMade(self):
        
        while True:
            
            self.n_bare_hx_made_today = 0
            self.n_bare_hy_made_today = 0

            self.n_powerboards_made_today = 0
            
            yield self.env.timeout(1)
                    

            
                                        
def ReadBarrelModuleRates( input_csv, list_of_production_sites):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        line = 0
        
        for row in csv_reader:
            if line == 0:
                line += 1
                continue

            for item in list_of_production_sites:
                if item.name == row[0]:

                    item.ls_rate = float(row[1])
                    item.ss_rate = float(row[2])
                    item.ls_target = int(row[3])
                    item.ss_target = int(row[4])


                    
                    
def ReadBarrelHybridRates(input_csv, list_of_production_sites):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')

        line = 0
        
        for row in csv_reader:
            if line == 0:
                line += 1
                continue
                
            for item in list_of_production_sites:
                
                if item.name == row[0]:
                    item.hybrid_production_capacity = float(row[1])
                    
def ReadBarrelYields( input_csv, list_of_production_sites, list_of_module_yields, list_of_hybrid_yields):
    
    with open(input_csv) as csv_file:
        csv_reader = csv.reader(csv_file, delimiter=',')
        
        line = 0

        for row in csv_reader:
            
            if line == 0:
                line += 1
                continue
            
            list_of_hybrid_yields.extend([float(row[0]),float(row[1])])
            list_of_module_yields.extend([float(row[2]),float(row[3])])

        for item in list_of_production_sites:
            item.ls_module_yield = list_of_module_yields[0]
            item.ss_module_yield = list_of_module_yields[1]
            item.hx_module_yield = list_of_hybrid_yields[0]
            item.hy_module_yield = list_of_hybrid_yields[1]
            

